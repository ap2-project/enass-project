﻿namespace Entity_FrameWork_Project
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.addbutton = new System.Windows.Forms.Button();
            this.updatebutton = new System.Windows.Forms.Button();
            this.deletebutton = new System.Windows.Forms.Button();
            this.selectbutton = new System.Windows.Forms.Button();
            this.exitbutton = new System.Windows.Forms.Button();
            this.advanced = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("DG Ghayaty", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(275, 120);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("DG Ghayaty", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(270, 60);
            this.label2.TabIndex = 1;
            this.label2.Text = "what do you want?";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // addbutton
            // 
            this.addbutton.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.addbutton.Font = new System.Drawing.Font("DG Ghayaty", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.addbutton.ForeColor = System.Drawing.SystemColors.Control;
            this.addbutton.Location = new System.Drawing.Point(22, 218);
            this.addbutton.Name = "addbutton";
            this.addbutton.Size = new System.Drawing.Size(132, 65);
            this.addbutton.TabIndex = 2;
            this.addbutton.Text = "Add";
            this.addbutton.UseVisualStyleBackColor = false;
            this.addbutton.Click += new System.EventHandler(this.addbutton_Click);
            // 
            // updatebutton
            // 
            this.updatebutton.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.updatebutton.Font = new System.Drawing.Font("DG Ghayaty", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.updatebutton.ForeColor = System.Drawing.SystemColors.Control;
            this.updatebutton.Location = new System.Drawing.Point(22, 318);
            this.updatebutton.Name = "updatebutton";
            this.updatebutton.Size = new System.Drawing.Size(132, 65);
            this.updatebutton.TabIndex = 3;
            this.updatebutton.Text = "Update";
            this.updatebutton.UseVisualStyleBackColor = false;
            this.updatebutton.Click += new System.EventHandler(this.updatebutton_Click);
            // 
            // deletebutton
            // 
            this.deletebutton.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.deletebutton.Font = new System.Drawing.Font("DG Ghayaty", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.deletebutton.ForeColor = System.Drawing.SystemColors.Control;
            this.deletebutton.Location = new System.Drawing.Point(22, 416);
            this.deletebutton.Name = "deletebutton";
            this.deletebutton.Size = new System.Drawing.Size(132, 65);
            this.deletebutton.TabIndex = 4;
            this.deletebutton.Text = "Delete";
            this.deletebutton.UseVisualStyleBackColor = false;
            this.deletebutton.Click += new System.EventHandler(this.deletebutton_Click);
            // 
            // selectbutton
            // 
            this.selectbutton.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.selectbutton.Font = new System.Drawing.Font("DG Ghayaty", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.selectbutton.ForeColor = System.Drawing.SystemColors.Control;
            this.selectbutton.Location = new System.Drawing.Point(22, 512);
            this.selectbutton.Name = "selectbutton";
            this.selectbutton.Size = new System.Drawing.Size(132, 65);
            this.selectbutton.TabIndex = 5;
            this.selectbutton.Text = "Select";
            this.selectbutton.UseVisualStyleBackColor = false;
            this.selectbutton.Click += new System.EventHandler(this.selectbutton_Click);
            // 
            // exitbutton
            // 
            this.exitbutton.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.exitbutton.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.exitbutton.ForeColor = System.Drawing.SystemColors.Control;
            this.exitbutton.Location = new System.Drawing.Point(911, 683);
            this.exitbutton.Name = "exitbutton";
            this.exitbutton.Size = new System.Drawing.Size(132, 47);
            this.exitbutton.TabIndex = 6;
            this.exitbutton.Text = "Exit";
            this.exitbutton.UseVisualStyleBackColor = false;
            this.exitbutton.Click += new System.EventHandler(this.exitbutton_Click);
            // 
            // advanced
            // 
            this.advanced.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.advanced.Font = new System.Drawing.Font("DG Ghayaty", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.advanced.ForeColor = System.Drawing.SystemColors.Control;
            this.advanced.Location = new System.Drawing.Point(22, 619);
            this.advanced.Name = "advanced";
            this.advanced.Size = new System.Drawing.Size(132, 65);
            this.advanced.TabIndex = 7;
            this.advanced.Text = "Advanced";
            this.advanced.UseVisualStyleBackColor = false;
            this.advanced.Click += new System.EventHandler(this.advanced_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(1055, 742);
            this.Controls.Add(this.advanced);
            this.Controls.Add(this.exitbutton);
            this.Controls.Add(this.selectbutton);
            this.Controls.Add(this.deletebutton);
            this.Controls.Add(this.updatebutton);
            this.Controls.Add(this.addbutton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "TCC management";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button addbutton;
        private System.Windows.Forms.Button updatebutton;
        private System.Windows.Forms.Button deletebutton;
        private System.Windows.Forms.Button selectbutton;
        private System.Windows.Forms.Button exitbutton;
        private System.Windows.Forms.Button advanced;
    }
}

