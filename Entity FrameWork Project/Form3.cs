﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Entity_FrameWork_Project
{
    public partial class Form3 : Form
    {
        TCCdbEntities db;
        public Form3()
        {
            InitializeComponent();
            db = new TCCdbEntities();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void studentupdatebtn_Click(object sender, EventArgs e)
        {
            Student.UpdateStudent(username.Text, firstname.Text , lastname.Text , email.Text, phone.Text , studentdepartment.Text );
        }

        //student search button
        private void button1_Click(object sender, EventArgs e)
        {
            Student student = db.Students.SingleOrDefault(s => s.UserName == username.Text);
            if (student != null)
            {
                Department dept = db.Departments.Find(student.DepartmentId);
                firstname.Text = student.FirstName;
                lastname.Text = student.LastName;
                email.Text = student.Email;
                phone.Text = student.Phone;
                studentdepartment.Text = dept.Name;
            }
            else MessageBox.Show("error");
        }

        private void departmentupdatebtn_Click(object sender, EventArgs e)
        {
            Department.UpdateDepartmentName(departmentname.Text, newdepartmentname.Text);
        }

        private void departmentsearchbtn_Click(object sender, EventArgs e)
        {
            Department department = db.Departments.SingleOrDefault(d => d.Name == departmentname.Text);
            if (department != null)
            {
                newdepartmentname.Text = department.Name;
            }
            else MessageBox.Show("not found");
        }

        private void subjectsearchbtn_Click(object sender, EventArgs e)
        {
            Subject subject = db.Subjects.SingleOrDefault(s => s.Name == subjectname.Text);
            if (subject != null)
            {
                subjectdepartment.Text = subject.Department.Name;
                subjectmindegree.Text = subject.MinimumDegree.ToString();
                subjectterm.Text = subject.Term.ToString();
                subjectyear.Text = subject.Year.ToString();
            }
            else MessageBox.Show("not found");
        }

        private void subjectupdatebtn_Click(object sender, EventArgs e)
        {
            Subject.UpdateSubject(subjectname.Text, subjectdepartment.Text , int.Parse(subjectmindegree.Text) , short.Parse(subjectterm.Text), short.Parse(subjectyear.Text) );
        }

        private void lecturesearchbtn_Click(object sender, EventArgs e)
        {
            SubjectLecture lecture = db.SubjectLectures.SingleOrDefault(l => l.Title == title.Text);
            if (lecture != null)
            {
                lecturesubject.Text = lecture.Subject.Name;
                lecturetitle.Text = lecture.Title;
                lecturecontent.Text = lecture.Content;
            }
            else MessageBox.Show("not found");
        }

        private void lecturesupdatebtn_Click(object sender, EventArgs e)
        {
            SubjectLecture.UpdateLecture(title.Text , lecturesubject.Text, lecturetitle.Text , lecturecontent.Text );
        }

        private void examsearchbtn_Click(object sender, EventArgs e)
        {
            Exam exam = db.Exams.SingleOrDefault(x => x.Subject.Name == examsubject.Text && x.Date == DateTime.Parse(examdate.Text));
            if (exam != null)
            {
                newexamdate.Text = exam.Date.ToString();
                newexamsubject.Text = exam.Subject.Name;
                examterm.Text = exam.Term.ToString();
            }
            else MessageBox.Show("not found");
        }

        private void examupdatebtn_Click(object sender, EventArgs e)
        {
            Exam.UpdateExam(examsubject.Text, DateTime.Parse(examdate.Text) , newexamsubject.Text, DateTime.Parse(newexamsubject.Text) , short.Parse(examterm.Text));
        }


        // those are some garbage!
        private void marksearchbtn_Click(object sender, EventArgs e)
        {
            Exam exam = db.Exams.Find(markexam.Text);
            Student student = db.Students.SingleOrDefault(s => s.UserName == markstudent.Text && s.StudentMarks.Any(m => m.ExamId == int.Parse(markexam.Text)));
            StudentMark stmark = db.StudentMarks.SingleOrDefault(m => m.StudentId == student.Id && m.ExamId == int.Parse(markexam.Text));

            if (exam != null)
            {
                if (student != null)
                {
                    if (mark != null)
                    {
                        newmarkstudent.Text = stmark.Student.UserName;
                        newmarkexam.Text = stmark.ExamId.ToString();
                        mark.Text = stmark.Mark.ToString();
                    }
                }
            }
            else MessageBox.Show("not found");
        }

        private void markupdatebtn_Click(object sender, EventArgs e)
        {
            StudentMark.UpdateMark(markstudent.Text, int.Parse(markexam.Text) , newmarkstudent.Text, int.Parse(newmarkexam.Text) , int.Parse(mark.Text));
        }

        private void backbtn_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            Form1 f1 = new Form1();
            f1.Show();
        }
    }
}