﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Entity_FrameWork_Project
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void showstudentstablebtn_Click(object sender, EventArgs e)
        {
            var table = Student.ShowStudentsTable();
            if (table != null)
                dataGridView1.DataSource = table;
            else
                MessageBox.Show("table is empty");
        }

 

        private void selectstudentbyid_Click(object sender, EventArgs e)
        {
            var student = Student.SelectStudentById(int.Parse(studentid.Text));

            if (student != null)
            {
                var dataTable = new DataTable();
                dataTable.Columns.Add("ID");
                dataTable.Columns.Add("User Name");
                dataTable.Columns.Add("First Name");
                dataTable.Columns.Add("Last Name");
                dataTable.Columns.Add("Email");
                dataTable.Columns.Add("Phone");
                dataTable.Columns.Add("Register Date");
                dataTable.Columns.Add("Department");

                var row = dataTable.NewRow();
                row["ID"] = student.Id;
                row["User Name"] = student.UserName;
                row["First Name"] = student.FirstName;
                row["Last Name"] = student.LastName;
                row["Email"] = student.Email;
                row["Phone"] = student.Phone;
                row["Register Date"] = student.RegisterDate;
                row["Department"] = student.Department.Name;
                dataTable.Rows.Add(row);

                dataGridView1.DataSource = dataTable;
            }
            else
                MessageBox.Show("not found!");
        }

        private void selectstudentbyusername_Click(object sender, EventArgs e)
        {
            var student = Student.SelectStudentByUserName(studentusername.Text);
            if (student != null)
            {
                var dataTable = new DataTable();
                dataTable.Columns.Add("ID");
                dataTable.Columns.Add("User Name");
                dataTable.Columns.Add("First Name");
                dataTable.Columns.Add("Last Name");
                dataTable.Columns.Add("Email");
                dataTable.Columns.Add("Phone");
                dataTable.Columns.Add("Register Date");
                dataTable.Columns.Add("Department");

                var row = dataTable.NewRow();
                row["ID"] = student.Id;
                row["User Name"] = student.UserName;
                row["First Name"] = student.FirstName;
                row["Last Name"] = student.LastName;
                row["Email"] = student.Email;
                row["Phone"] = student.Phone;
                row["Register Date"] = student.RegisterDate;
                row["Department"] = student.Department.Name;
                dataTable.Rows.Add(row);

                dataGridView1.DataSource = dataTable;
            }
            else
                MessageBox.Show("not found!");
        }

        private void selectstudentbyemail_Click(object sender, EventArgs e)
        {
            var student = Student.SelectStudentByEmail(studentemail.Text);
            if (student != null)
            {
                var dataTable = new DataTable();
                dataTable.Columns.Add("ID");
                dataTable.Columns.Add("User Name");
                dataTable.Columns.Add("First Name");
                dataTable.Columns.Add("Last Name");
                dataTable.Columns.Add("Email");
                dataTable.Columns.Add("Phone");
                dataTable.Columns.Add("Register Date");
                dataTable.Columns.Add("Department");

                var row = dataTable.NewRow();
                row["ID"] = student.Id;
                row["User Name"] = student.UserName;
                row["First Name"] = student.FirstName;
                row["Last Name"] = student.LastName;
                row["Email"] = student.Email;
                row["Phone"] = student.Phone;
                row["Register Date"] = student.RegisterDate;
                row["Department"] = student.Department.Name;
                dataTable.Rows.Add(row);

                dataGridView1.DataSource = dataTable;
            }
            else
                MessageBox.Show("not found!");
        }
    

        private void selectstudentbyphone_Click(object sender, EventArgs e)
        {
            var student = Student.SelectStudentByPhone(studentphone.Text);
            if (student != null)
            {
                var dataTable = new DataTable();
                dataTable.Columns.Add("ID");
                dataTable.Columns.Add("User Name");
                dataTable.Columns.Add("First Name");
                dataTable.Columns.Add("Last Name");
                dataTable.Columns.Add("Email");
                dataTable.Columns.Add("Phone");
                dataTable.Columns.Add("Register Date");
                dataTable.Columns.Add("Department");

                var row = dataTable.NewRow();
                row["ID"] = student.Id;
                row["User Name"] = student.UserName;
                row["First Name"] = student.FirstName;
                row["Last Name"] = student.LastName;
                row["Email"] = student.Email;
                row["Phone"] = student.Phone;
                row["Register Date"] = student.RegisterDate;
                row["Department"] = student.Department.Name;
                dataTable.Rows.Add(row);

                dataGridView1.DataSource = dataTable;
            }
            else
                MessageBox.Show("not found!");
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void showexamtable_Click(object sender, EventArgs e)
        {
            var table = Exam.ShowExamsTable();
            if (table != null)
                dataGridView1.DataSource = table;
            else
                MessageBox.Show("table is empty!");
        }

        private void selectexambyid_Click(object sender, EventArgs e)
        {
            var exam = Exam.SelectExamById(int.Parse(examid.Text));
            if (exam != null)
            {
                var dataTable = new DataTable();
                dataTable.Columns.Add("ID");
                dataTable.Columns.Add("Subject");
                dataTable.Columns.Add("Date");
                dataTable.Columns.Add("Term");

                var row = dataTable.NewRow();
                row["ID"] = exam.Id;
                row["Subject"] = exam.Subject.Name;
                row["Date"] = exam.Date;
                row["Term"] = exam.Term;
                
                dataTable.Rows.Add(row);

                dataGridView1.DataSource = dataTable;
            }
            else
                MessageBox.Show("not found!");
        }

        private void selectexambysubject_Click(object sender, EventArgs e)
        {
            var exam = Exam.SelectExamBySubject(examsubject.Text);
            if (exam != null)
                dataGridView1.DataSource = exam;
            else
                MessageBox.Show("not found!");
        }

        private void selectexambyterm_Click(object sender, EventArgs e)
        {
            var exam = Exam.SelectExamByTerm(int.Parse(examterm.Text));
            if (exam != null)
                dataGridView1.DataSource = exam;
            else
                MessageBox.Show("not found!");
        }

        private void selectmarkbyexam_Click(object sender, EventArgs e)
        {
            var mark = StudentMark.SelectMarksByExam(int.Parse(markexam.Text));
            if (mark != null)
                dataGridView1.DataSource = mark;
            else
                MessageBox.Show("not found!");
        }

        private void selectmarkbystudent_Click(object sender, EventArgs e)
        {
            var mark = StudentMark.SelectMarksByStudent(markstudent.Text);
            if (mark != null)
                dataGridView1.DataSource = mark;
            else
                MessageBox.Show("not found!");
        }

        private void showmakstable_Click(object sender, EventArgs e)
        {
            var table = StudentMark.ShowSTudentMarksTable();
            if (table != null)
                dataGridView1.DataSource = table;
            else
                MessageBox.Show("table is empty");
        }

        private void showsubjecttable_Click(object sender, EventArgs e)
        {
            var table = Subject.ShowSubjectsTable();
            if (table != null)
                dataGridView1.DataSource = table;
            else
                MessageBox.Show("table is empty");
        }

        private void selectsubjectbyname_Click(object sender, EventArgs e)
        {
            var subject = Subject.SelectSubjectByName(subjectname.Text);
            if (subject != null)
            {
                var dataTable = new DataTable();
                dataTable.Columns.Add("ID");
                dataTable.Columns.Add("Name");
                dataTable.Columns.Add("Department");
                dataTable.Columns.Add("Minimum Degree");
                dataTable.Columns.Add("Term");
                dataTable.Columns.Add("Year");

                var row = dataTable.NewRow();
                row["ID"] = subject.Id;
                row["Name"] = subject.Name;
                row["Department"] = subject.Department.Name;
                row["Minimum Degree"] = subject.MinimumDegree;
                row["Term"] = subject.Term;
                row["Year"] = subject.Year;

                dataTable.Rows.Add(row);

                dataGridView1.DataSource = dataTable;
            }
            else
                MessageBox.Show("not found!");
        }

        private void selectsubjectbydept_Click(object sender, EventArgs e)
        {
            var subject = Subject.SelectSubjectByDepartment(subjectdepartment.Text);
            if (subject != null)
                dataGridView1.DataSource = subject;
            else
                MessageBox.Show("not found!");
        }

        private void selectsubjectbymindeg_Click(object sender, EventArgs e)
        {
            var subject = Subject.SelectSubjectByMinimumDegree(int.Parse(subjectmindeg.Text));
            if (subject != null)
                dataGridView1.DataSource = subject;
            else
                MessageBox.Show("not found!");
        }

        private void selectsubjectbyterm_Click(object sender, EventArgs e)
        {
            var subject = Subject.SelectSubjectByTerm(short.Parse(subjectterm.Text));
            if (subject != null)
                dataGridView1.DataSource = subject;
            else
                MessageBox.Show("not found!");
        }

        private void selectlecturebyid_Click(object sender, EventArgs e)
        {
            var lecture = SubjectLecture.SelectLectureById(int.Parse(lectureid.Text));
            if (lecture != null)
            {
                var dataTable = new DataTable();
                dataTable.Columns.Add("ID");
                dataTable.Columns.Add("Subject");
                dataTable.Columns.Add("Title");
                dataTable.Columns.Add("Content");
                
                var row = dataTable.NewRow();
                row["ID"] = lecture.Id;
                row["Subject"] = lecture.Subject.Name;
                row["Title"] = lecture.Title;
                row["Content"] = lecture.Content;

                dataTable.Rows.Add(row);

                dataGridView1.DataSource = dataTable;
            }
            else
                MessageBox.Show("not found!");
        }

        private void selectlecturebytitle_Click(object sender, EventArgs e)
        {
            var lecture = SubjectLecture.SelectLectureByTitle(lecturetitle.Text);
            if (lecture != null)
            {
                var dataTable = new DataTable();
                dataTable.Columns.Add("ID");
                dataTable.Columns.Add("Subject");
                dataTable.Columns.Add("Title");
                dataTable.Columns.Add("Content");

                var row = dataTable.NewRow();
                row["ID"] = lecture.Id;
                row["Subject"] = lecture.Subject.Name;
                row["Title"] = lecture.Title;
                row["Content"] = lecture.Content;

                dataTable.Rows.Add(row);

                dataGridView1.DataSource = dataTable;
            }
            else
                MessageBox.Show("not found!");
        }

        private void selectlecturebysubject_Click(object sender, EventArgs e)
        {
            var lecture = SubjectLecture.SelectLectureBySubject(lecturesubject.Text);
            if (lecture != null)
                dataGridView1.DataSource = lecture;
            else
                MessageBox.Show("not found!");
        }

        private void showdepttable_Click(object sender, EventArgs e)
        {
            var depts = Department.ShowDepartmentTable();
            if (depts != null)
                dataGridView1.DataSource = depts;
            else
                MessageBox.Show("not found!");
        }

        private void selectdeptbyid_Click(object sender, EventArgs e)
        {
            var dept = Department.SelectDepartmentById(int.Parse(deptid.Text));
            if (dept != null)
            {
                var dataTable = new DataTable();
                dataTable.Columns.Add("ID");
                dataTable.Columns.Add("Name");

                var row = dataTable.NewRow();
                row["ID"] = dept.Id;
                row["Name"] = dept.Name;

                dataTable.Rows.Add(row);

                dataGridView1.DataSource = dataTable;
            }
            else
                MessageBox.Show("not found!");
        }

        private void selectdeptbyname_Click(object sender, EventArgs e)
        {
            var dept = Department.SelectDepartmentByName(deptname.Text);
            if (dept != null)
            {
                var dataTable = new DataTable();
                dataTable.Columns.Add("ID");
                dataTable.Columns.Add("Name");

                var row = dataTable.NewRow();
                row["ID"] = dept.Id;
                row["Name"] = dept.Name;

                dataTable.Rows.Add(row);

                dataGridView1.DataSource = dataTable;
            }
            else
                MessageBox.Show("not found!");
        }

        private void backbtn_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            Form1 f1 = new Form1();
            f1.Show();
        }
    }
}
