﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Entity_FrameWork_Project
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void studentaddbtn_Click(object sender, EventArgs e)
        {
            Student.AddStudent(username.Text, firstname.Text, lastname.Text, email.Text, phone.Text, int.Parse(registerdate.Text) , studentdepartment.Text);
        }

        private void departmentaddbtn_Click(object sender, EventArgs e)
        {
            Department.AddDepartment(departmentname.Text);
        }

        private void subjectaddbtn_Click(object sender, EventArgs e)
        {
            Subject.AddSubject(subjectname.Text, subjectdepartment.Text, int.Parse(subjectmindegree.Text), short.Parse(subjectterm.Text), short.Parse(subjectyear.Text));
        }

        private void lecturesupdatebtn_Click(object sender, EventArgs e)
        {
            SubjectLecture.AddSubjectLecture(lecturesubject.Text, lecturetitle.Text, lecturecontent.Text);
        }

        private void examaddbtn_Click(object sender, EventArgs e)
        {
            Exam.AddExam(examsubject.Text, DateTime.Parse(examdate.Text) , short.Parse(examterm.Text));
        }

        private void markaddbtn_Click(object sender, EventArgs e)
        {
            StudentMark.AddMark(markstudent.Text, int.Parse(markexam.Text) , int.Parse(mark.Text));
        }

        private void backbtn_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            Form1 f1 = new Form1();
            f1.Show();
        }
    }
}
