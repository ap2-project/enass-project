﻿namespace Entity_FrameWork_Project
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form4));
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.deletelecturebytitle = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.lecturetitle = new System.Windows.Forms.TextBox();
            this.deletelecturebyid = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.lectureid = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.deleteexambyid = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.examid = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.deletemarkbyid = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.markid = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.deletesubjectbyid = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.subjectid = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.deletedepartmentbyname = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.departmentbyname = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.deletestudentbyemail = new System.Windows.Forms.Button();
            this.deletestudetnbyuser = new System.Windows.Forms.Button();
            this.deletestudentbyid = new System.Windows.Forms.Button();
            this.studentemail = new System.Windows.Forms.TextBox();
            this.studentuser = new System.Windows.Forms.TextBox();
            this.studentid = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.backbtn = new System.Windows.Forms.Button();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.deletelecturebytitle);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.lecturetitle);
            this.groupBox4.Controls.Add(this.deletelecturebyid);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.lectureid);
            this.groupBox4.Font = new System.Drawing.Font("DG Ghayaty", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(680, 16);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox4.Size = new System.Drawing.Size(357, 449);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Delete Subject Lecture";
            // 
            // deletelecturebytitle
            // 
            this.deletelecturebytitle.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.deletelecturebytitle.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deletelecturebytitle.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.deletelecturebytitle.Location = new System.Drawing.Point(241, 259);
            this.deletelecturebytitle.Name = "deletelecturebytitle";
            this.deletelecturebytitle.Size = new System.Drawing.Size(101, 43);
            this.deletelecturebytitle.TabIndex = 21;
            this.deletelecturebytitle.Text = "Delete";
            this.deletelecturebytitle.UseVisualStyleBackColor = false;
            this.deletelecturebytitle.Click += new System.EventHandler(this.deletelecturebytitle_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("DG Ghayaty", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(11, 186);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(135, 40);
            this.label10.TabIndex = 20;
            this.label10.Text = "Delete By Title";
            // 
            // lecturetitle
            // 
            this.lecturetitle.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lecturetitle.Location = new System.Drawing.Point(18, 229);
            this.lecturetitle.Multiline = true;
            this.lecturetitle.Name = "lecturetitle";
            this.lecturetitle.Size = new System.Drawing.Size(160, 28);
            this.lecturetitle.TabIndex = 19;
            // 
            // deletelecturebyid
            // 
            this.deletelecturebyid.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.deletelecturebyid.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deletelecturebyid.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.deletelecturebyid.Location = new System.Drawing.Point(245, 124);
            this.deletelecturebyid.Name = "deletelecturebyid";
            this.deletelecturebyid.Size = new System.Drawing.Size(101, 43);
            this.deletelecturebyid.TabIndex = 18;
            this.deletelecturebyid.Text = "Delete";
            this.deletelecturebyid.UseVisualStyleBackColor = false;
            this.deletelecturebyid.Click += new System.EventHandler(this.deletelecturebyid_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("DG Ghayaty", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 40);
            this.label4.TabIndex = 1;
            this.label4.Text = "Delete By Id";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // lectureid
            // 
            this.lectureid.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lectureid.Location = new System.Drawing.Point(22, 94);
            this.lectureid.Multiline = true;
            this.lectureid.Name = "lectureid";
            this.lectureid.Size = new System.Drawing.Size(160, 28);
            this.lectureid.TabIndex = 0;
            this.lectureid.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.deleteexambyid);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.examid);
            this.groupBox5.Font = new System.Drawing.Font("DG Ghayaty", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(680, 466);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox5.Size = new System.Drawing.Size(357, 260);
            this.groupBox5.TabIndex = 15;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Delete Exam";
            // 
            // deleteexambyid
            // 
            this.deleteexambyid.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.deleteexambyid.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteexambyid.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.deleteexambyid.Location = new System.Drawing.Point(234, 181);
            this.deleteexambyid.Name = "deleteexambyid";
            this.deleteexambyid.Size = new System.Drawing.Size(101, 43);
            this.deleteexambyid.TabIndex = 18;
            this.deleteexambyid.Text = "Delete";
            this.deleteexambyid.UseVisualStyleBackColor = false;
            this.deleteexambyid.Click += new System.EventHandler(this.deleteexambyid_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("DG Ghayaty", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(11, 64);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(114, 40);
            this.label8.TabIndex = 1;
            this.label8.Text = "Delete By Id";
            // 
            // examid
            // 
            this.examid.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.examid.Location = new System.Drawing.Point(18, 110);
            this.examid.Multiline = true;
            this.examid.Name = "examid";
            this.examid.Size = new System.Drawing.Size(160, 28);
            this.examid.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.deletemarkbyid);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this.markid);
            this.groupBox6.Font = new System.Drawing.Font("DG Ghayaty", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(341, 259);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox6.Size = new System.Drawing.Size(309, 260);
            this.groupBox6.TabIndex = 16;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Delete Subject Mark";
            // 
            // deletemarkbyid
            // 
            this.deletemarkbyid.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.deletemarkbyid.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deletemarkbyid.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.deletemarkbyid.Location = new System.Drawing.Point(197, 181);
            this.deletemarkbyid.Name = "deletemarkbyid";
            this.deletemarkbyid.Size = new System.Drawing.Size(101, 43);
            this.deletemarkbyid.TabIndex = 18;
            this.deletemarkbyid.Text = "Delete";
            this.deletemarkbyid.UseVisualStyleBackColor = false;
            this.deletemarkbyid.Click += new System.EventHandler(this.deletemarkbyid_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("DG Ghayaty", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(11, 62);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 40);
            this.label9.TabIndex = 1;
            this.label9.Text = "Delete By Id";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // markid
            // 
            this.markid.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.markid.Location = new System.Drawing.Point(12, 105);
            this.markid.Multiline = true;
            this.markid.Name = "markid";
            this.markid.Size = new System.Drawing.Size(160, 28);
            this.markid.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.deletesubjectbyid);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.subjectid);
            this.groupBox3.Font = new System.Drawing.Font("DG Ghayaty", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(341, 16);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox3.Size = new System.Drawing.Size(309, 213);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Delete Subject";
            // 
            // deletesubjectbyid
            // 
            this.deletesubjectbyid.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.deletesubjectbyid.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deletesubjectbyid.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.deletesubjectbyid.Location = new System.Drawing.Point(197, 124);
            this.deletesubjectbyid.Name = "deletesubjectbyid";
            this.deletesubjectbyid.Size = new System.Drawing.Size(101, 43);
            this.deletesubjectbyid.TabIndex = 2;
            this.deletesubjectbyid.Text = "Delete";
            this.deletesubjectbyid.UseVisualStyleBackColor = false;
            this.deletesubjectbyid.Click += new System.EventHandler(this.deletesubjectbyid_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("DG Ghayaty", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(18, 45);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(114, 40);
            this.label7.TabIndex = 1;
            this.label7.Text = "Delete By Id";
            // 
            // subjectid
            // 
            this.subjectid.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectid.Location = new System.Drawing.Point(25, 88);
            this.subjectid.Multiline = true;
            this.subjectid.Name = "subjectid";
            this.subjectid.Size = new System.Drawing.Size(160, 28);
            this.subjectid.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.deletedepartmentbyname);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.departmentbyname);
            this.groupBox2.Font = new System.Drawing.Font("DG Ghayaty", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(14, 466);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox2.Size = new System.Drawing.Size(290, 260);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Delete Department";
            // 
            // deletedepartmentbyname
            // 
            this.deletedepartmentbyname.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.deletedepartmentbyname.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deletedepartmentbyname.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.deletedepartmentbyname.Location = new System.Drawing.Point(178, 192);
            this.deletedepartmentbyname.Name = "deletedepartmentbyname";
            this.deletedepartmentbyname.Size = new System.Drawing.Size(101, 43);
            this.deletedepartmentbyname.TabIndex = 17;
            this.deletedepartmentbyname.Text = "Delete";
            this.deletedepartmentbyname.UseVisualStyleBackColor = false;
            this.deletedepartmentbyname.Click += new System.EventHandler(this.deletedepartmentbyname_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("DG Ghayaty", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 64);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 40);
            this.label6.TabIndex = 1;
            this.label6.Text = "Delete By Id";
            // 
            // departmentbyname
            // 
            this.departmentbyname.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.departmentbyname.Location = new System.Drawing.Point(18, 107);
            this.departmentbyname.Multiline = true;
            this.departmentbyname.Name = "departmentbyname";
            this.departmentbyname.Size = new System.Drawing.Size(160, 28);
            this.departmentbyname.TabIndex = 0;
            this.departmentbyname.TextChanged += new System.EventHandler(this.textBox6_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.deletestudentbyemail);
            this.groupBox1.Controls.Add(this.deletestudetnbyuser);
            this.groupBox1.Controls.Add(this.deletestudentbyid);
            this.groupBox1.Controls.Add(this.studentemail);
            this.groupBox1.Controls.Add(this.studentuser);
            this.groupBox1.Controls.Add(this.studentid);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("DG Ghayaty", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(14, 16);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox1.Size = new System.Drawing.Size(304, 449);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Delete Student";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // deletestudentbyemail
            // 
            this.deletestudentbyemail.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.deletestudentbyemail.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deletestudentbyemail.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.deletestudentbyemail.Location = new System.Drawing.Point(193, 377);
            this.deletestudentbyemail.Name = "deletestudentbyemail";
            this.deletestudentbyemail.Size = new System.Drawing.Size(101, 43);
            this.deletestudentbyemail.TabIndex = 14;
            this.deletestudentbyemail.Text = "Delete";
            this.deletestudentbyemail.UseVisualStyleBackColor = false;
            this.deletestudentbyemail.Click += new System.EventHandler(this.deletestudentbyemail_Click);
            // 
            // deletestudetnbyuser
            // 
            this.deletestudetnbyuser.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.deletestudetnbyuser.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deletestudetnbyuser.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.deletestudetnbyuser.Location = new System.Drawing.Point(193, 258);
            this.deletestudetnbyuser.Name = "deletestudetnbyuser";
            this.deletestudetnbyuser.Size = new System.Drawing.Size(101, 43);
            this.deletestudetnbyuser.TabIndex = 13;
            this.deletestudetnbyuser.Text = "Delete";
            this.deletestudetnbyuser.UseVisualStyleBackColor = false;
            this.deletestudetnbyuser.Click += new System.EventHandler(this.deletestudetnbyuser_Click);
            // 
            // deletestudentbyid
            // 
            this.deletestudentbyid.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.deletestudentbyid.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deletestudentbyid.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.deletestudentbyid.Location = new System.Drawing.Point(192, 124);
            this.deletestudentbyid.Name = "deletestudentbyid";
            this.deletestudentbyid.Size = new System.Drawing.Size(101, 43);
            this.deletestudentbyid.TabIndex = 12;
            this.deletestudentbyid.Text = "Delete";
            this.deletestudentbyid.UseVisualStyleBackColor = false;
            this.deletestudentbyid.Click += new System.EventHandler(this.deletestudentbyid_Click);
            // 
            // studentemail
            // 
            this.studentemail.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentemail.Location = new System.Drawing.Point(27, 338);
            this.studentemail.Multiline = true;
            this.studentemail.Name = "studentemail";
            this.studentemail.Size = new System.Drawing.Size(160, 28);
            this.studentemail.TabIndex = 11;
            // 
            // studentuser
            // 
            this.studentuser.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentuser.Location = new System.Drawing.Point(27, 225);
            this.studentuser.Multiline = true;
            this.studentuser.Name = "studentuser";
            this.studentuser.Size = new System.Drawing.Size(160, 28);
            this.studentuser.TabIndex = 10;
            // 
            // studentid
            // 
            this.studentid.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentid.Location = new System.Drawing.Point(27, 90);
            this.studentid.Multiline = true;
            this.studentid.Name = "studentid";
            this.studentid.Size = new System.Drawing.Size(160, 28);
            this.studentid.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(21, 299);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 36);
            this.label3.TabIndex = 6;
            this.label3.Text = "Delete By Email";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(21, 186);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(174, 36);
            this.label2.TabIndex = 3;
            this.label2.Text = "Delete By User Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 36);
            this.label1.TabIndex = 1;
            this.label1.Text = "Delete By Id";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // backbtn
            // 
            this.backbtn.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.backbtn.Font = new System.Drawing.Font("DG Ghayaty", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backbtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.backbtn.Location = new System.Drawing.Point(12, 696);
            this.backbtn.Name = "backbtn";
            this.backbtn.Size = new System.Drawing.Size(58, 34);
            this.backbtn.TabIndex = 22;
            this.backbtn.Text = "back";
            this.backbtn.UseVisualStyleBackColor = false;
            this.backbtn.Click += new System.EventHandler(this.backbtn_Click);
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(1055, 742);
            this.Controls.Add(this.backbtn);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form4";
            this.Text = "Delete";
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox lectureid;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox examid;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox markid;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button deletesubjectbyid;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox subjectid;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox departmentbyname;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox studentid;
        private System.Windows.Forms.Button deletelecturebyid;
        private System.Windows.Forms.Button deleteexambyid;
        private System.Windows.Forms.Button deletemarkbyid;
        private System.Windows.Forms.Button deletedepartmentbyname;
        private System.Windows.Forms.Button deletestudentbyemail;
        private System.Windows.Forms.Button deletestudetnbyuser;
        private System.Windows.Forms.Button deletestudentbyid;
        private System.Windows.Forms.TextBox studentemail;
        private System.Windows.Forms.TextBox studentuser;
        private System.Windows.Forms.Button deletelecturebytitle;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox lecturetitle;
        private System.Windows.Forms.Button backbtn;
    }
}