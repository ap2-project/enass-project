﻿namespace Entity_FrameWork_Project
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form3));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.studentsearchbtn = new System.Windows.Forms.Button();
            this.studentdepartment = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.phone = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.email = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lastname = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.firstname = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.studentupdatebtn = new System.Windows.Forms.Button();
            this.username = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.subjectsearchbtn = new System.Windows.Forms.Button();
            this.subjectname = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.subjectyear = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.subjectterm = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.subjectmindegree = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.subjectdepartment = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.subjectupdatebtn = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.newexamdate = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.newexamsubject = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.examsearchbtn = new System.Windows.Forms.Button();
            this.examterm = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.examdate = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.examsubject = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.examupdatebtn = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.newdepartmentname = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.departmentsearchbtn = new System.Windows.Forms.Button();
            this.departmentname = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.departmentupdatebtn = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lecturesearchbtn = new System.Windows.Forms.Button();
            this.title = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.lecturecontent = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.lecturetitle = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.lecturesubject = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.lecturesupdatebtn = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.marksearchbtn = new System.Windows.Forms.Button();
            this.markexam = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.markstudent = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.mark = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.newmarkexam = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.newmarkstudent = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.markupdatebtn = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.backbtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.studentsearchbtn);
            this.groupBox1.Controls.Add(this.studentdepartment);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.groupBox7);
            this.groupBox1.Controls.Add(this.phone);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.email);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lastname);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.firstname);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.studentupdatebtn);
            this.groupBox1.Controls.Add(this.username);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("DG Ghayaty", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(17, 2);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox1.Size = new System.Drawing.Size(304, 473);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Update Student";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // studentsearchbtn
            // 
            this.studentsearchbtn.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.studentsearchbtn.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentsearchbtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.studentsearchbtn.Location = new System.Drawing.Point(192, 110);
            this.studentsearchbtn.Name = "studentsearchbtn";
            this.studentsearchbtn.Size = new System.Drawing.Size(101, 43);
            this.studentsearchbtn.TabIndex = 23;
            this.studentsearchbtn.Text = "Search";
            this.studentsearchbtn.UseVisualStyleBackColor = false;
            this.studentsearchbtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // studentdepartment
            // 
            this.studentdepartment.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentdepartment.Location = new System.Drawing.Point(133, 360);
            this.studentdepartment.Multiline = true;
            this.studentdepartment.Name = "studentdepartment";
            this.studentdepartment.Size = new System.Drawing.Size(160, 28);
            this.studentdepartment.TabIndex = 22;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(17, 352);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(116, 36);
            this.label7.TabIndex = 21;
            this.label7.Text = "Department:";
            // 
            // phone
            // 
            this.phone.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phone.Location = new System.Drawing.Point(133, 313);
            this.phone.Multiline = true;
            this.phone.Name = "phone";
            this.phone.Size = new System.Drawing.Size(160, 28);
            this.phone.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(17, 305);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 36);
            this.label5.TabIndex = 13;
            this.label5.Text = "phone:";
            // 
            // email
            // 
            this.email.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.email.Location = new System.Drawing.Point(133, 260);
            this.email.Multiline = true;
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(160, 28);
            this.email.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(17, 252);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 36);
            this.label4.TabIndex = 17;
            this.label4.Text = "email:";
            // 
            // lastname
            // 
            this.lastname.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lastname.Location = new System.Drawing.Point(133, 212);
            this.lastname.Multiline = true;
            this.lastname.Name = "lastname";
            this.lastname.Size = new System.Drawing.Size(160, 28);
            this.lastname.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 204);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 36);
            this.label3.TabIndex = 13;
            this.label3.Text = "last name:";
            // 
            // firstname
            // 
            this.firstname.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstname.Location = new System.Drawing.Point(133, 168);
            this.firstname.Multiline = true;
            this.firstname.Name = "firstname";
            this.firstname.Size = new System.Drawing.Size(160, 28);
            this.firstname.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 160);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 36);
            this.label2.TabIndex = 15;
            this.label2.Text = "first name:";
            // 
            // studentupdatebtn
            // 
            this.studentupdatebtn.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.studentupdatebtn.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentupdatebtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.studentupdatebtn.Location = new System.Drawing.Point(192, 420);
            this.studentupdatebtn.Name = "studentupdatebtn";
            this.studentupdatebtn.Size = new System.Drawing.Size(101, 43);
            this.studentupdatebtn.TabIndex = 14;
            this.studentupdatebtn.Text = "Update";
            this.studentupdatebtn.UseVisualStyleBackColor = false;
            this.studentupdatebtn.Click += new System.EventHandler(this.studentupdatebtn_Click);
            // 
            // username
            // 
            this.username.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.username.Location = new System.Drawing.Point(133, 65);
            this.username.Multiline = true;
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(160, 28);
            this.username.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 36);
            this.label1.TabIndex = 1;
            this.label1.Text = "user name:";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.subjectsearchbtn);
            this.groupBox2.Controls.Add(this.subjectname);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.subjectyear);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.subjectterm);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.subjectmindegree);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.subjectdepartment);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.subjectupdatebtn);
            this.groupBox2.Font = new System.Drawing.Font("DG Ghayaty", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(373, 2);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox2.Size = new System.Drawing.Size(318, 378);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Update Subject";
            // 
            // subjectsearchbtn
            // 
            this.subjectsearchbtn.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.subjectsearchbtn.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectsearchbtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.subjectsearchbtn.Location = new System.Drawing.Point(206, 99);
            this.subjectsearchbtn.Name = "subjectsearchbtn";
            this.subjectsearchbtn.Size = new System.Drawing.Size(101, 43);
            this.subjectsearchbtn.TabIndex = 25;
            this.subjectsearchbtn.Text = "Search";
            this.subjectsearchbtn.UseVisualStyleBackColor = false;
            this.subjectsearchbtn.Click += new System.EventHandler(this.subjectsearchbtn_Click);
            // 
            // subjectname
            // 
            this.subjectname.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectname.Location = new System.Drawing.Point(147, 65);
            this.subjectname.Multiline = true;
            this.subjectname.Name = "subjectname";
            this.subjectname.Size = new System.Drawing.Size(160, 28);
            this.subjectname.TabIndex = 24;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(14, 57);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(63, 36);
            this.label15.TabIndex = 23;
            this.label15.Text = "name:";
            // 
            // subjectyear
            // 
            this.subjectyear.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectyear.Location = new System.Drawing.Point(147, 281);
            this.subjectyear.Multiline = true;
            this.subjectyear.Name = "subjectyear";
            this.subjectyear.Size = new System.Drawing.Size(160, 28);
            this.subjectyear.TabIndex = 22;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(14, 275);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 36);
            this.label9.TabIndex = 21;
            this.label9.Text = "Year:";
            // 
            // subjectterm
            // 
            this.subjectterm.AcceptsReturn = true;
            this.subjectterm.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectterm.Location = new System.Drawing.Point(147, 242);
            this.subjectterm.Multiline = true;
            this.subjectterm.Name = "subjectterm";
            this.subjectterm.Size = new System.Drawing.Size(160, 28);
            this.subjectterm.TabIndex = 20;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(14, 236);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 36);
            this.label8.TabIndex = 19;
            this.label8.Text = "Term:";
            // 
            // subjectmindegree
            // 
            this.subjectmindegree.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectmindegree.Location = new System.Drawing.Point(147, 196);
            this.subjectmindegree.Multiline = true;
            this.subjectmindegree.Name = "subjectmindegree";
            this.subjectmindegree.Size = new System.Drawing.Size(160, 28);
            this.subjectmindegree.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(14, 190);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(104, 36);
            this.label10.TabIndex = 13;
            this.label10.Text = "min degree:";
            // 
            // subjectdepartment
            // 
            this.subjectdepartment.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectdepartment.Location = new System.Drawing.Point(147, 150);
            this.subjectdepartment.Multiline = true;
            this.subjectdepartment.Name = "subjectdepartment";
            this.subjectdepartment.Size = new System.Drawing.Size(160, 28);
            this.subjectdepartment.TabIndex = 18;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(17, 142);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(116, 36);
            this.label11.TabIndex = 17;
            this.label11.Text = "Department:";
            // 
            // subjectupdatebtn
            // 
            this.subjectupdatebtn.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.subjectupdatebtn.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectupdatebtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.subjectupdatebtn.Location = new System.Drawing.Point(206, 325);
            this.subjectupdatebtn.Name = "subjectupdatebtn";
            this.subjectupdatebtn.Size = new System.Drawing.Size(101, 43);
            this.subjectupdatebtn.TabIndex = 14;
            this.subjectupdatebtn.Text = "Update";
            this.subjectupdatebtn.UseVisualStyleBackColor = false;
            this.subjectupdatebtn.Click += new System.EventHandler(this.subjectupdatebtn_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.newexamdate);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.newexamsubject);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.examsearchbtn);
            this.groupBox3.Controls.Add(this.examterm);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.examdate);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.examsubject);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.examupdatebtn);
            this.groupBox3.Font = new System.Drawing.Font("DG Ghayaty", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(723, 2);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox3.Size = new System.Drawing.Size(299, 361);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Update Exam";
            // 
            // newexamdate
            // 
            this.newexamdate.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newexamdate.Location = new System.Drawing.Point(133, 234);
            this.newexamdate.Multiline = true;
            this.newexamdate.Name = "newexamdate";
            this.newexamdate.Size = new System.Drawing.Size(160, 28);
            this.newexamdate.TabIndex = 25;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(17, 226);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 36);
            this.label12.TabIndex = 24;
            this.label12.Text = "Date:";
            // 
            // newexamsubject
            // 
            this.newexamsubject.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newexamsubject.Location = new System.Drawing.Point(133, 196);
            this.newexamsubject.Multiline = true;
            this.newexamsubject.Name = "newexamsubject";
            this.newexamsubject.Size = new System.Drawing.Size(160, 28);
            this.newexamsubject.TabIndex = 23;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(14, 188);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(78, 36);
            this.label25.TabIndex = 22;
            this.label25.Text = "Subject:";
            // 
            // examsearchbtn
            // 
            this.examsearchbtn.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.examsearchbtn.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.examsearchbtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.examsearchbtn.Location = new System.Drawing.Point(187, 130);
            this.examsearchbtn.Name = "examsearchbtn";
            this.examsearchbtn.Size = new System.Drawing.Size(101, 43);
            this.examsearchbtn.TabIndex = 21;
            this.examsearchbtn.Text = "Search";
            this.examsearchbtn.UseVisualStyleBackColor = false;
            this.examsearchbtn.Click += new System.EventHandler(this.examsearchbtn_Click);
            // 
            // examterm
            // 
            this.examterm.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.examterm.Location = new System.Drawing.Point(133, 274);
            this.examterm.Multiline = true;
            this.examterm.Name = "examterm";
            this.examterm.Size = new System.Drawing.Size(160, 28);
            this.examterm.TabIndex = 20;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(14, 268);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(62, 36);
            this.label14.TabIndex = 19;
            this.label14.Text = "Term:";
            // 
            // examdate
            // 
            this.examdate.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.examdate.Location = new System.Drawing.Point(128, 96);
            this.examdate.Multiline = true;
            this.examdate.Name = "examdate";
            this.examdate.Size = new System.Drawing.Size(160, 28);
            this.examdate.TabIndex = 18;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(17, 88);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(60, 36);
            this.label16.TabIndex = 17;
            this.label16.Text = "Date:";
            // 
            // examsubject
            // 
            this.examsubject.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.examsubject.Location = new System.Drawing.Point(128, 58);
            this.examsubject.Multiline = true;
            this.examsubject.Name = "examsubject";
            this.examsubject.Size = new System.Drawing.Size(160, 28);
            this.examsubject.TabIndex = 14;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(14, 50);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(78, 36);
            this.label17.TabIndex = 13;
            this.label17.Text = "Subject:";
            // 
            // examupdatebtn
            // 
            this.examupdatebtn.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.examupdatebtn.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.examupdatebtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.examupdatebtn.Location = new System.Drawing.Point(192, 309);
            this.examupdatebtn.Name = "examupdatebtn";
            this.examupdatebtn.Size = new System.Drawing.Size(101, 43);
            this.examupdatebtn.TabIndex = 14;
            this.examupdatebtn.Text = "Update";
            this.examupdatebtn.UseVisualStyleBackColor = false;
            this.examupdatebtn.Click += new System.EventHandler(this.examupdatebtn_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.newdepartmentname);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.departmentsearchbtn);
            this.groupBox4.Controls.Add(this.departmentname);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.departmentupdatebtn);
            this.groupBox4.Font = new System.Drawing.Font("DG Ghayaty", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(17, 489);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox4.Size = new System.Drawing.Size(304, 237);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Update Department";
            // 
            // newdepartmentname
            // 
            this.newdepartmentname.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newdepartmentname.Location = new System.Drawing.Point(133, 144);
            this.newdepartmentname.Multiline = true;
            this.newdepartmentname.Name = "newdepartmentname";
            this.newdepartmentname.Size = new System.Drawing.Size(160, 28);
            this.newdepartmentname.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(14, 136);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 36);
            this.label6.TabIndex = 16;
            this.label6.Text = "new name:";
            // 
            // departmentsearchbtn
            // 
            this.departmentsearchbtn.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.departmentsearchbtn.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.departmentsearchbtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.departmentsearchbtn.Location = new System.Drawing.Point(192, 95);
            this.departmentsearchbtn.Name = "departmentsearchbtn";
            this.departmentsearchbtn.Size = new System.Drawing.Size(101, 43);
            this.departmentsearchbtn.TabIndex = 15;
            this.departmentsearchbtn.Text = "Search";
            this.departmentsearchbtn.UseVisualStyleBackColor = false;
            this.departmentsearchbtn.Click += new System.EventHandler(this.departmentsearchbtn_Click);
            // 
            // departmentname
            // 
            this.departmentname.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.departmentname.Location = new System.Drawing.Point(133, 59);
            this.departmentname.Multiline = true;
            this.departmentname.Name = "departmentname";
            this.departmentname.Size = new System.Drawing.Size(160, 28);
            this.departmentname.TabIndex = 14;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(14, 51);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(88, 36);
            this.label20.TabIndex = 13;
            this.label20.Text = "old name:";
            // 
            // departmentupdatebtn
            // 
            this.departmentupdatebtn.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.departmentupdatebtn.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.departmentupdatebtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.departmentupdatebtn.Location = new System.Drawing.Point(192, 184);
            this.departmentupdatebtn.Name = "departmentupdatebtn";
            this.departmentupdatebtn.Size = new System.Drawing.Size(101, 43);
            this.departmentupdatebtn.TabIndex = 14;
            this.departmentupdatebtn.Text = "Update";
            this.departmentupdatebtn.UseVisualStyleBackColor = false;
            this.departmentupdatebtn.Click += new System.EventHandler(this.departmentupdatebtn_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.lecturesearchbtn);
            this.groupBox5.Controls.Add(this.title);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.lecturecontent);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.lecturetitle);
            this.groupBox5.Controls.Add(this.label19);
            this.groupBox5.Controls.Add(this.lecturesubject);
            this.groupBox5.Controls.Add(this.label21);
            this.groupBox5.Controls.Add(this.lecturesupdatebtn);
            this.groupBox5.Font = new System.Drawing.Font("DG Ghayaty", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(373, 394);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox5.Size = new System.Drawing.Size(318, 332);
            this.groupBox5.TabIndex = 16;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Update Lectures";
            // 
            // lecturesearchbtn
            // 
            this.lecturesearchbtn.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.lecturesearchbtn.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lecturesearchbtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lecturesearchbtn.Location = new System.Drawing.Point(206, 100);
            this.lecturesearchbtn.Name = "lecturesearchbtn";
            this.lecturesearchbtn.Size = new System.Drawing.Size(101, 43);
            this.lecturesearchbtn.TabIndex = 21;
            this.lecturesearchbtn.Text = "Search";
            this.lecturesearchbtn.UseVisualStyleBackColor = false;
            this.lecturesearchbtn.Click += new System.EventHandler(this.lecturesearchbtn_Click);
            // 
            // title
            // 
            this.title.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title.Location = new System.Drawing.Point(147, 59);
            this.title.Multiline = true;
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(160, 28);
            this.title.TabIndex = 20;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(14, 51);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 36);
            this.label13.TabIndex = 19;
            this.label13.Text = "Title:";
            // 
            // lecturecontent
            // 
            this.lecturecontent.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lecturecontent.Location = new System.Drawing.Point(147, 245);
            this.lecturecontent.Multiline = true;
            this.lecturecontent.Name = "lecturecontent";
            this.lecturecontent.Size = new System.Drawing.Size(160, 28);
            this.lecturecontent.TabIndex = 14;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(14, 239);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 36);
            this.label18.TabIndex = 13;
            this.label18.Text = "Content:";
            // 
            // lecturetitle
            // 
            this.lecturetitle.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lecturetitle.Location = new System.Drawing.Point(147, 200);
            this.lecturetitle.Multiline = true;
            this.lecturetitle.Name = "lecturetitle";
            this.lecturetitle.Size = new System.Drawing.Size(160, 28);
            this.lecturetitle.TabIndex = 18;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(17, 192);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(55, 36);
            this.label19.TabIndex = 17;
            this.label19.Text = "Title:";
            // 
            // lecturesubject
            // 
            this.lecturesubject.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lecturesubject.Location = new System.Drawing.Point(147, 156);
            this.lecturesubject.Multiline = true;
            this.lecturesubject.Name = "lecturesubject";
            this.lecturesubject.Size = new System.Drawing.Size(160, 28);
            this.lecturesubject.TabIndex = 14;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(14, 148);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(78, 36);
            this.label21.TabIndex = 13;
            this.label21.Text = "Subject:";
            // 
            // lecturesupdatebtn
            // 
            this.lecturesupdatebtn.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.lecturesupdatebtn.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lecturesupdatebtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lecturesupdatebtn.Location = new System.Drawing.Point(206, 281);
            this.lecturesupdatebtn.Name = "lecturesupdatebtn";
            this.lecturesupdatebtn.Size = new System.Drawing.Size(101, 43);
            this.lecturesupdatebtn.TabIndex = 14;
            this.lecturesupdatebtn.Text = "Update";
            this.lecturesupdatebtn.UseVisualStyleBackColor = false;
            this.lecturesupdatebtn.Click += new System.EventHandler(this.lecturesupdatebtn_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.marksearchbtn);
            this.groupBox6.Controls.Add(this.markexam);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this.markstudent);
            this.groupBox6.Controls.Add(this.label27);
            this.groupBox6.Controls.Add(this.mark);
            this.groupBox6.Controls.Add(this.label22);
            this.groupBox6.Controls.Add(this.newmarkexam);
            this.groupBox6.Controls.Add(this.label23);
            this.groupBox6.Controls.Add(this.newmarkstudent);
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Controls.Add(this.markupdatebtn);
            this.groupBox6.Font = new System.Drawing.Font("DG Ghayaty", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(723, 364);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox6.Size = new System.Drawing.Size(299, 362);
            this.groupBox6.TabIndex = 17;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Update Marks ";
            // 
            // marksearchbtn
            // 
            this.marksearchbtn.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.marksearchbtn.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.marksearchbtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.marksearchbtn.Location = new System.Drawing.Point(192, 132);
            this.marksearchbtn.Name = "marksearchbtn";
            this.marksearchbtn.Size = new System.Drawing.Size(101, 43);
            this.marksearchbtn.TabIndex = 23;
            this.marksearchbtn.Text = "Search";
            this.marksearchbtn.UseVisualStyleBackColor = false;
            this.marksearchbtn.Click += new System.EventHandler(this.marksearchbtn_Click);
            // 
            // markexam
            // 
            this.markexam.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.markexam.Location = new System.Drawing.Point(133, 98);
            this.markexam.Multiline = true;
            this.markexam.Name = "markexam";
            this.markexam.Size = new System.Drawing.Size(160, 28);
            this.markexam.TabIndex = 22;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(14, 90);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(61, 36);
            this.label26.TabIndex = 21;
            this.label26.Text = "Exam:";
            // 
            // markstudent
            // 
            this.markstudent.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.markstudent.Location = new System.Drawing.Point(133, 57);
            this.markstudent.Multiline = true;
            this.markstudent.Name = "markstudent";
            this.markstudent.Size = new System.Drawing.Size(160, 28);
            this.markstudent.TabIndex = 20;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(14, 49);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(76, 36);
            this.label27.TabIndex = 19;
            this.label27.Text = "Student";
            // 
            // mark
            // 
            this.mark.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mark.Location = new System.Drawing.Point(133, 273);
            this.mark.Multiline = true;
            this.mark.Name = "mark";
            this.mark.Size = new System.Drawing.Size(160, 28);
            this.mark.TabIndex = 14;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(14, 267);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(60, 36);
            this.label22.TabIndex = 13;
            this.label22.Text = "Mark:";
            // 
            // newmarkexam
            // 
            this.newmarkexam.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newmarkexam.Location = new System.Drawing.Point(133, 236);
            this.newmarkexam.Multiline = true;
            this.newmarkexam.Name = "newmarkexam";
            this.newmarkexam.Size = new System.Drawing.Size(160, 28);
            this.newmarkexam.TabIndex = 18;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(14, 228);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(61, 36);
            this.label23.TabIndex = 17;
            this.label23.Text = "Exam:";
            // 
            // newmarkstudent
            // 
            this.newmarkstudent.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newmarkstudent.Location = new System.Drawing.Point(133, 195);
            this.newmarkstudent.Multiline = true;
            this.newmarkstudent.Name = "newmarkstudent";
            this.newmarkstudent.Size = new System.Drawing.Size(160, 28);
            this.newmarkstudent.TabIndex = 14;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(14, 187);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(76, 36);
            this.label24.TabIndex = 13;
            this.label24.Text = "Student";
            // 
            // markupdatebtn
            // 
            this.markupdatebtn.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.markupdatebtn.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.markupdatebtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.markupdatebtn.Location = new System.Drawing.Point(187, 311);
            this.markupdatebtn.Name = "markupdatebtn";
            this.markupdatebtn.Size = new System.Drawing.Size(101, 43);
            this.markupdatebtn.TabIndex = 14;
            this.markupdatebtn.Text = "Update";
            this.markupdatebtn.UseVisualStyleBackColor = false;
            this.markupdatebtn.Click += new System.EventHandler(this.markupdatebtn_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.Transparent;
            this.groupBox7.Controls.Add(this.textBox1);
            this.groupBox7.Controls.Add(this.label28);
            this.groupBox7.Controls.Add(this.button1);
            this.groupBox7.Controls.Add(this.textBox2);
            this.groupBox7.Controls.Add(this.label29);
            this.groupBox7.Controls.Add(this.button2);
            this.groupBox7.Font = new System.Drawing.Font("DG Ghayaty", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(0, 461);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox7.Size = new System.Drawing.Size(304, 237);
            this.groupBox7.TabIndex = 15;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Update Department";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(133, 144);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(160, 28);
            this.textBox1.TabIndex = 17;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(14, 136);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(97, 36);
            this.label28.TabIndex = 16;
            this.label28.Text = "new name:";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button1.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(192, 95);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(101, 43);
            this.button1.TabIndex = 15;
            this.button1.Text = "Search";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.departmentsearchbtn_Click);
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(133, 59);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(160, 28);
            this.textBox2.TabIndex = 14;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(14, 51);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(88, 36);
            this.label29.TabIndex = 13;
            this.label29.Text = "old name:";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button2.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Location = new System.Drawing.Point(192, 184);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(101, 43);
            this.button2.TabIndex = 14;
            this.button2.Text = "Update";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.departmentupdatebtn_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.Transparent;
            this.groupBox8.Controls.Add(this.button3);
            this.groupBox8.Controls.Add(this.textBox3);
            this.groupBox8.Controls.Add(this.label30);
            this.groupBox8.Controls.Add(this.textBox4);
            this.groupBox8.Controls.Add(this.label31);
            this.groupBox8.Controls.Add(this.textBox5);
            this.groupBox8.Controls.Add(this.label32);
            this.groupBox8.Controls.Add(this.textBox6);
            this.groupBox8.Controls.Add(this.label33);
            this.groupBox8.Controls.Add(this.textBox7);
            this.groupBox8.Controls.Add(this.label34);
            this.groupBox8.Controls.Add(this.button4);
            this.groupBox8.Font = new System.Drawing.Font("DG Ghayaty", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.Location = new System.Drawing.Point(373, 2);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox8.Size = new System.Drawing.Size(318, 378);
            this.groupBox8.TabIndex = 13;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Update Subject";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button3.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.Location = new System.Drawing.Point(206, 99);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(101, 43);
            this.button3.TabIndex = 25;
            this.button3.Text = "Search";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.subjectsearchbtn_Click);
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(147, 65);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(160, 28);
            this.textBox3.TabIndex = 24;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(14, 57);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(63, 36);
            this.label30.TabIndex = 23;
            this.label30.Text = "name:";
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(147, 281);
            this.textBox4.Multiline = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(160, 28);
            this.textBox4.TabIndex = 22;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(14, 275);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(55, 36);
            this.label31.TabIndex = 21;
            this.label31.Text = "Year:";
            // 
            // textBox5
            // 
            this.textBox5.AcceptsReturn = true;
            this.textBox5.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.Location = new System.Drawing.Point(147, 242);
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(160, 28);
            this.textBox5.TabIndex = 20;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(14, 236);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(62, 36);
            this.label32.TabIndex = 19;
            this.label32.Text = "Term:";
            // 
            // textBox6
            // 
            this.textBox6.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(147, 196);
            this.textBox6.Multiline = true;
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(160, 28);
            this.textBox6.TabIndex = 14;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(14, 190);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(104, 36);
            this.label33.TabIndex = 13;
            this.label33.Text = "min degree:";
            // 
            // textBox7
            // 
            this.textBox7.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.Location = new System.Drawing.Point(147, 150);
            this.textBox7.Multiline = true;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(160, 28);
            this.textBox7.TabIndex = 18;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(17, 142);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(116, 36);
            this.label34.TabIndex = 17;
            this.label34.Text = "Department:";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button4.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Location = new System.Drawing.Point(206, 325);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(101, 43);
            this.button4.TabIndex = 14;
            this.button4.Text = "Update";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.subjectupdatebtn_Click);
            // 
            // backbtn
            // 
            this.backbtn.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.backbtn.Font = new System.Drawing.Font("DG Ghayaty", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backbtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.backbtn.Location = new System.Drawing.Point(12, 696);
            this.backbtn.Name = "backbtn";
            this.backbtn.Size = new System.Drawing.Size(58, 34);
            this.backbtn.TabIndex = 22;
            this.backbtn.Text = "back";
            this.backbtn.UseVisualStyleBackColor = false;
            this.backbtn.Click += new System.EventHandler(this.backbtn_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(1055, 742);
            this.Controls.Add(this.backbtn);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form3";
            this.Text = "Update";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button studentupdatebtn;
        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox studentdepartment;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox phone;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox email;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox lastname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox firstname;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox subjectyear;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox subjectterm;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox subjectmindegree;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox subjectdepartment;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button subjectupdatebtn;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox examterm;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox examdate;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox examsubject;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button examupdatebtn;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox departmentname;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button departmentupdatebtn;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox lecturecontent;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox lecturetitle;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox lecturesubject;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button lecturesupdatebtn;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox mark;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox newmarkexam;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox newmarkstudent;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button markupdatebtn;
        private System.Windows.Forms.Button studentsearchbtn;
        private System.Windows.Forms.TextBox newdepartmentname;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button departmentsearchbtn;
        private System.Windows.Forms.TextBox title;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button subjectsearchbtn;
        private System.Windows.Forms.TextBox subjectname;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox newexamdate;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox newexamsubject;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button examsearchbtn;
        private System.Windows.Forms.Button lecturesearchbtn;
        private System.Windows.Forms.Button marksearchbtn;
        private System.Windows.Forms.TextBox markexam;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox markstudent;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button backbtn;
    }
}