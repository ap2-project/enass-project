﻿namespace Entity_FrameWork_Project
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form5));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.showstudentstablebtn = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.selectstudentbyphone = new System.Windows.Forms.Button();
            this.studentphone = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.selectstudentbyemail = new System.Windows.Forms.Button();
            this.studentemail = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.selectstudentbyusername = new System.Windows.Forms.Button();
            this.studentusername = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.selectstudentbyid = new System.Windows.Forms.Button();
            this.studentid = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.selectexambyterm = new System.Windows.Forms.Button();
            this.examterm = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.selectexambysubject = new System.Windows.Forms.Button();
            this.examsubject = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.selectexambyid = new System.Windows.Forms.Button();
            this.examid = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.showexamtable = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.showmakstable = new System.Windows.Forms.Button();
            this.selectmarkbystudent = new System.Windows.Forms.Button();
            this.markstudent = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.selectmarkbyexam = new System.Windows.Forms.Button();
            this.markexam = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.selectsubjectbyterm = new System.Windows.Forms.Button();
            this.subjectterm = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.selectsubjectbymindeg = new System.Windows.Forms.Button();
            this.subjectmindeg = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.selectsubjectbydept = new System.Windows.Forms.Button();
            this.subjectdepartment = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.selectsubjectbyname = new System.Windows.Forms.Button();
            this.subjectname = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.showsubjecttable = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.selectlecturebysubject = new System.Windows.Forms.Button();
            this.lecturesubject = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.selectlecturebytitle = new System.Windows.Forms.Button();
            this.lecturetitle = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.selectlecturebyid = new System.Windows.Forms.Button();
            this.lectureid = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.showlecturetable = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.showdepttable = new System.Windows.Forms.Button();
            this.selectdeptbyname = new System.Windows.Forms.Button();
            this.deptname = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.selectdeptbyid = new System.Windows.Forms.Button();
            this.deptid = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.backbtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Menu;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridView1.Location = new System.Drawing.Point(574, 22);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(572, 605);
            this.dataGridView1.TabIndex = 0;
            // 
            // showstudentstablebtn
            // 
            this.showstudentstablebtn.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.showstudentstablebtn.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showstudentstablebtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.showstudentstablebtn.Location = new System.Drawing.Point(17, 473);
            this.showstudentstablebtn.Name = "showstudentstablebtn";
            this.showstudentstablebtn.Size = new System.Drawing.Size(209, 43);
            this.showstudentstablebtn.TabIndex = 15;
            this.showstudentstablebtn.Text = "Show Students Table";
            this.showstudentstablebtn.UseVisualStyleBackColor = false;
            this.showstudentstablebtn.Click += new System.EventHandler(this.showstudentstablebtn_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.selectstudentbyphone);
            this.groupBox4.Controls.Add(this.studentphone);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.selectstudentbyemail);
            this.groupBox4.Controls.Add(this.studentemail);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.selectstudentbyusername);
            this.groupBox4.Controls.Add(this.studentusername);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.selectstudentbyid);
            this.groupBox4.Controls.Add(this.studentid);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.showstudentstablebtn);
            this.groupBox4.Font = new System.Drawing.Font("DG Ghayaty", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(17, 6);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox4.Size = new System.Drawing.Size(247, 522);
            this.groupBox4.TabIndex = 16;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Student Table";
            // 
            // selectstudentbyphone
            // 
            this.selectstudentbyphone.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.selectstudentbyphone.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectstudentbyphone.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectstudentbyphone.Location = new System.Drawing.Point(142, 411);
            this.selectstudentbyphone.Name = "selectstudentbyphone";
            this.selectstudentbyphone.Size = new System.Drawing.Size(84, 43);
            this.selectstudentbyphone.TabIndex = 27;
            this.selectstudentbyphone.Text = "Select";
            this.selectstudentbyphone.UseVisualStyleBackColor = false;
            this.selectstudentbyphone.Click += new System.EventHandler(this.selectstudentbyphone_Click);
            // 
            // studentphone
            // 
            this.studentphone.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentphone.Location = new System.Drawing.Point(94, 377);
            this.studentphone.Multiline = true;
            this.studentphone.Name = "studentphone";
            this.studentphone.Size = new System.Drawing.Size(132, 28);
            this.studentphone.TabIndex = 26;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1, 369);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 36);
            this.label4.TabIndex = 25;
            this.label4.Text = "Phone:";
            // 
            // selectstudentbyemail
            // 
            this.selectstudentbyemail.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.selectstudentbyemail.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectstudentbyemail.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectstudentbyemail.Location = new System.Drawing.Point(142, 311);
            this.selectstudentbyemail.Name = "selectstudentbyemail";
            this.selectstudentbyemail.Size = new System.Drawing.Size(84, 43);
            this.selectstudentbyemail.TabIndex = 24;
            this.selectstudentbyemail.Text = "Select";
            this.selectstudentbyemail.UseVisualStyleBackColor = false;
            this.selectstudentbyemail.Click += new System.EventHandler(this.selectstudentbyemail_Click);
            // 
            // studentemail
            // 
            this.studentemail.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentemail.Location = new System.Drawing.Point(94, 277);
            this.studentemail.Multiline = true;
            this.studentemail.Name = "studentemail";
            this.studentemail.Size = new System.Drawing.Size(132, 28);
            this.studentemail.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1, 269);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 36);
            this.label3.TabIndex = 22;
            this.label3.Text = "Email:";
            // 
            // selectstudentbyusername
            // 
            this.selectstudentbyusername.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.selectstudentbyusername.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectstudentbyusername.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectstudentbyusername.Location = new System.Drawing.Point(152, 202);
            this.selectstudentbyusername.Name = "selectstudentbyusername";
            this.selectstudentbyusername.Size = new System.Drawing.Size(84, 43);
            this.selectstudentbyusername.TabIndex = 21;
            this.selectstudentbyusername.Text = "Select";
            this.selectstudentbyusername.UseVisualStyleBackColor = false;
            this.selectstudentbyusername.Click += new System.EventHandler(this.selectstudentbyusername_Click);
            // 
            // studentusername
            // 
            this.studentusername.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentusername.Location = new System.Drawing.Point(104, 168);
            this.studentusername.Multiline = true;
            this.studentusername.Name = "studentusername";
            this.studentusername.Size = new System.Drawing.Size(132, 28);
            this.studentusername.TabIndex = 20;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 160);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 36);
            this.label2.TabIndex = 19;
            this.label2.Text = "Username:";
            // 
            // selectstudentbyid
            // 
            this.selectstudentbyid.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.selectstudentbyid.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectstudentbyid.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectstudentbyid.Location = new System.Drawing.Point(152, 91);
            this.selectstudentbyid.Name = "selectstudentbyid";
            this.selectstudentbyid.Size = new System.Drawing.Size(84, 43);
            this.selectstudentbyid.TabIndex = 18;
            this.selectstudentbyid.Text = "Select";
            this.selectstudentbyid.UseVisualStyleBackColor = false;
            this.selectstudentbyid.Click += new System.EventHandler(this.selectstudentbyid_Click);
            // 
            // studentid
            // 
            this.studentid.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentid.Location = new System.Drawing.Point(104, 57);
            this.studentid.Multiline = true;
            this.studentid.Name = "studentid";
            this.studentid.Size = new System.Drawing.Size(132, 28);
            this.studentid.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 36);
            this.label1.TabIndex = 16;
            this.label1.Text = "Id:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.selectexambyterm);
            this.groupBox1.Controls.Add(this.examterm);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.selectexambysubject);
            this.groupBox1.Controls.Add(this.examsubject);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.selectexambyid);
            this.groupBox1.Controls.Add(this.examid);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.showexamtable);
            this.groupBox1.Font = new System.Drawing.Font("DG Ghayaty", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(280, 6);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox1.Size = new System.Drawing.Size(247, 485);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Exam Table";
            // 
            // selectexambyterm
            // 
            this.selectexambyterm.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.selectexambyterm.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectexambyterm.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectexambyterm.Location = new System.Drawing.Point(152, 360);
            this.selectexambyterm.Name = "selectexambyterm";
            this.selectexambyterm.Size = new System.Drawing.Size(84, 43);
            this.selectexambyterm.TabIndex = 24;
            this.selectexambyterm.Text = "Select";
            this.selectexambyterm.UseVisualStyleBackColor = false;
            this.selectexambyterm.Click += new System.EventHandler(this.selectexambyterm_Click);
            // 
            // examterm
            // 
            this.examterm.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.examterm.Location = new System.Drawing.Point(104, 326);
            this.examterm.Multiline = true;
            this.examterm.Name = "examterm";
            this.examterm.Size = new System.Drawing.Size(132, 28);
            this.examterm.TabIndex = 23;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 318);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 36);
            this.label6.TabIndex = 22;
            this.label6.Text = "Term";
            // 
            // selectexambysubject
            // 
            this.selectexambysubject.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.selectexambysubject.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectexambysubject.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectexambysubject.Location = new System.Drawing.Point(152, 224);
            this.selectexambysubject.Name = "selectexambysubject";
            this.selectexambysubject.Size = new System.Drawing.Size(84, 43);
            this.selectexambysubject.TabIndex = 21;
            this.selectexambysubject.Text = "Select";
            this.selectexambysubject.UseVisualStyleBackColor = false;
            this.selectexambysubject.Click += new System.EventHandler(this.selectexambysubject_Click);
            // 
            // examsubject
            // 
            this.examsubject.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.examsubject.Location = new System.Drawing.Point(104, 190);
            this.examsubject.Multiline = true;
            this.examsubject.Name = "examsubject";
            this.examsubject.Size = new System.Drawing.Size(132, 28);
            this.examsubject.TabIndex = 20;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(11, 182);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 36);
            this.label7.TabIndex = 19;
            this.label7.Text = "Subject:";
            // 
            // selectexambyid
            // 
            this.selectexambyid.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.selectexambyid.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectexambyid.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectexambyid.Location = new System.Drawing.Point(152, 91);
            this.selectexambyid.Name = "selectexambyid";
            this.selectexambyid.Size = new System.Drawing.Size(84, 43);
            this.selectexambyid.TabIndex = 18;
            this.selectexambyid.Text = "Select";
            this.selectexambyid.UseVisualStyleBackColor = false;
            this.selectexambyid.Click += new System.EventHandler(this.selectexambyid_Click);
            // 
            // examid
            // 
            this.examid.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.examid.Location = new System.Drawing.Point(104, 57);
            this.examid.Multiline = true;
            this.examid.Name = "examid";
            this.examid.Size = new System.Drawing.Size(132, 28);
            this.examid.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(11, 49);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 36);
            this.label8.TabIndex = 16;
            this.label8.Text = "Id:";
            // 
            // showexamtable
            // 
            this.showexamtable.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.showexamtable.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showexamtable.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.showexamtable.Location = new System.Drawing.Point(17, 432);
            this.showexamtable.Name = "showexamtable";
            this.showexamtable.Size = new System.Drawing.Size(209, 43);
            this.showexamtable.TabIndex = 15;
            this.showexamtable.Text = "Show Exam Table";
            this.showexamtable.UseVisualStyleBackColor = false;
            this.showexamtable.Click += new System.EventHandler(this.showexamtable_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.showmakstable);
            this.groupBox2.Controls.Add(this.selectmarkbystudent);
            this.groupBox2.Controls.Add(this.markstudent);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.selectmarkbyexam);
            this.groupBox2.Controls.Add(this.markexam);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Font = new System.Drawing.Font("DG Ghayaty", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(574, 661);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox2.Size = new System.Drawing.Size(247, 295);
            this.groupBox2.TabIndex = 18;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Marks Table";
            // 
            // showmakstable
            // 
            this.showmakstable.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.showmakstable.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showmakstable.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.showmakstable.Location = new System.Drawing.Point(17, 242);
            this.showmakstable.Name = "showmakstable";
            this.showmakstable.Size = new System.Drawing.Size(209, 43);
            this.showmakstable.TabIndex = 22;
            this.showmakstable.Text = "Show Marks Table";
            this.showmakstable.UseVisualStyleBackColor = false;
            this.showmakstable.Click += new System.EventHandler(this.showmakstable_Click);
            // 
            // selectmarkbystudent
            // 
            this.selectmarkbystudent.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.selectmarkbystudent.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectmarkbystudent.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectmarkbystudent.Location = new System.Drawing.Point(152, 185);
            this.selectmarkbystudent.Name = "selectmarkbystudent";
            this.selectmarkbystudent.Size = new System.Drawing.Size(84, 43);
            this.selectmarkbystudent.TabIndex = 21;
            this.selectmarkbystudent.Text = "Select";
            this.selectmarkbystudent.UseVisualStyleBackColor = false;
            this.selectmarkbystudent.Click += new System.EventHandler(this.selectmarkbystudent_Click);
            // 
            // markstudent
            // 
            this.markstudent.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.markstudent.Location = new System.Drawing.Point(104, 151);
            this.markstudent.Multiline = true;
            this.markstudent.Name = "markstudent";
            this.markstudent.Size = new System.Drawing.Size(132, 28);
            this.markstudent.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(11, 143);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 36);
            this.label9.TabIndex = 19;
            this.label9.Text = "Student:";
            // 
            // selectmarkbyexam
            // 
            this.selectmarkbyexam.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.selectmarkbyexam.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectmarkbyexam.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectmarkbyexam.Location = new System.Drawing.Point(152, 91);
            this.selectmarkbyexam.Name = "selectmarkbyexam";
            this.selectmarkbyexam.Size = new System.Drawing.Size(84, 43);
            this.selectmarkbyexam.TabIndex = 18;
            this.selectmarkbyexam.Text = "Select";
            this.selectmarkbyexam.UseVisualStyleBackColor = false;
            this.selectmarkbyexam.Click += new System.EventHandler(this.selectmarkbyexam_Click);
            // 
            // markexam
            // 
            this.markexam.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.markexam.Location = new System.Drawing.Point(104, 57);
            this.markexam.Multiline = true;
            this.markexam.Name = "markexam";
            this.markexam.Size = new System.Drawing.Size(132, 28);
            this.markexam.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(11, 49);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 36);
            this.label10.TabIndex = 16;
            this.label10.Text = "Exam Id:";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button4.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Location = new System.Drawing.Point(17, 342);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(209, 43);
            this.button4.TabIndex = 15;
            this.button4.Text = "Show Exam Table";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.selectsubjectbyterm);
            this.groupBox3.Controls.Add(this.subjectterm);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.selectsubjectbymindeg);
            this.groupBox3.Controls.Add(this.subjectmindeg);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.selectsubjectbydept);
            this.groupBox3.Controls.Add(this.subjectdepartment);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.selectsubjectbyname);
            this.groupBox3.Controls.Add(this.subjectname);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.showsubjecttable);
            this.groupBox3.Font = new System.Drawing.Font("DG Ghayaty", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(280, 494);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox3.Size = new System.Drawing.Size(247, 447);
            this.groupBox3.TabIndex = 19;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Subject Table:";
            // 
            // selectsubjectbyterm
            // 
            this.selectsubjectbyterm.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.selectsubjectbyterm.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectsubjectbyterm.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectsubjectbyterm.Location = new System.Drawing.Point(152, 344);
            this.selectsubjectbyterm.Name = "selectsubjectbyterm";
            this.selectsubjectbyterm.Size = new System.Drawing.Size(84, 43);
            this.selectsubjectbyterm.TabIndex = 27;
            this.selectsubjectbyterm.Text = "Select";
            this.selectsubjectbyterm.UseVisualStyleBackColor = false;
            this.selectsubjectbyterm.Click += new System.EventHandler(this.selectsubjectbyterm_Click);
            // 
            // subjectterm
            // 
            this.subjectterm.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectterm.Location = new System.Drawing.Point(104, 310);
            this.subjectterm.Multiline = true;
            this.subjectterm.Name = "subjectterm";
            this.subjectterm.Size = new System.Drawing.Size(132, 28);
            this.subjectterm.TabIndex = 26;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(11, 302);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 36);
            this.label5.TabIndex = 25;
            this.label5.Text = "Term:";
            // 
            // selectsubjectbymindeg
            // 
            this.selectsubjectbymindeg.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.selectsubjectbymindeg.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectsubjectbymindeg.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectsubjectbymindeg.Location = new System.Drawing.Point(152, 258);
            this.selectsubjectbymindeg.Name = "selectsubjectbymindeg";
            this.selectsubjectbymindeg.Size = new System.Drawing.Size(84, 43);
            this.selectsubjectbymindeg.TabIndex = 24;
            this.selectsubjectbymindeg.Text = "Select";
            this.selectsubjectbymindeg.UseVisualStyleBackColor = false;
            this.selectsubjectbymindeg.Click += new System.EventHandler(this.selectsubjectbymindeg_Click);
            // 
            // subjectmindeg
            // 
            this.subjectmindeg.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectmindeg.Location = new System.Drawing.Point(104, 224);
            this.subjectmindeg.Multiline = true;
            this.subjectmindeg.Name = "subjectmindeg";
            this.subjectmindeg.Size = new System.Drawing.Size(132, 28);
            this.subjectmindeg.TabIndex = 23;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(11, 216);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(102, 36);
            this.label11.TabIndex = 22;
            this.label11.Text = "MinDegree:";
            // 
            // selectsubjectbydept
            // 
            this.selectsubjectbydept.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.selectsubjectbydept.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectsubjectbydept.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectsubjectbydept.Location = new System.Drawing.Point(152, 172);
            this.selectsubjectbydept.Name = "selectsubjectbydept";
            this.selectsubjectbydept.Size = new System.Drawing.Size(84, 43);
            this.selectsubjectbydept.TabIndex = 21;
            this.selectsubjectbydept.Text = "Select";
            this.selectsubjectbydept.UseVisualStyleBackColor = false;
            this.selectsubjectbydept.Click += new System.EventHandler(this.selectsubjectbydept_Click);
            // 
            // subjectdepartment
            // 
            this.subjectdepartment.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectdepartment.Location = new System.Drawing.Point(104, 138);
            this.subjectdepartment.Multiline = true;
            this.subjectdepartment.Name = "subjectdepartment";
            this.subjectdepartment.Size = new System.Drawing.Size(132, 28);
            this.subjectdepartment.TabIndex = 20;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(11, 130);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 36);
            this.label12.TabIndex = 19;
            this.label12.Text = "Dept:";
            // 
            // selectsubjectbyname
            // 
            this.selectsubjectbyname.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.selectsubjectbyname.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectsubjectbyname.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectsubjectbyname.Location = new System.Drawing.Point(152, 91);
            this.selectsubjectbyname.Name = "selectsubjectbyname";
            this.selectsubjectbyname.Size = new System.Drawing.Size(84, 43);
            this.selectsubjectbyname.TabIndex = 18;
            this.selectsubjectbyname.Text = "Select";
            this.selectsubjectbyname.UseVisualStyleBackColor = false;
            this.selectsubjectbyname.Click += new System.EventHandler(this.selectsubjectbyname_Click);
            // 
            // subjectname
            // 
            this.subjectname.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectname.Location = new System.Drawing.Point(104, 57);
            this.subjectname.Multiline = true;
            this.subjectname.Name = "subjectname";
            this.subjectname.Size = new System.Drawing.Size(132, 28);
            this.subjectname.TabIndex = 17;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(11, 49);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 36);
            this.label13.TabIndex = 16;
            this.label13.Text = "Name:";
            // 
            // showsubjecttable
            // 
            this.showsubjecttable.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.showsubjecttable.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showsubjecttable.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.showsubjecttable.Location = new System.Drawing.Point(17, 393);
            this.showsubjecttable.Name = "showsubjecttable";
            this.showsubjecttable.Size = new System.Drawing.Size(209, 43);
            this.showsubjecttable.TabIndex = 15;
            this.showsubjecttable.Text = "Show Subject Table";
            this.showsubjecttable.UseVisualStyleBackColor = false;
            this.showsubjecttable.Click += new System.EventHandler(this.showsubjecttable_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.selectlecturebysubject);
            this.groupBox5.Controls.Add(this.lecturesubject);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.selectlecturebytitle);
            this.groupBox5.Controls.Add(this.lecturetitle);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.selectlecturebyid);
            this.groupBox5.Controls.Add(this.lectureid);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Controls.Add(this.showlecturetable);
            this.groupBox5.Font = new System.Drawing.Font("DG Ghayaty", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(17, 537);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox5.Size = new System.Drawing.Size(247, 404);
            this.groupBox5.TabIndex = 20;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Lecture Table";
            // 
            // selectlecturebysubject
            // 
            this.selectlecturebysubject.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.selectlecturebysubject.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectlecturebysubject.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectlecturebysubject.Location = new System.Drawing.Point(152, 276);
            this.selectlecturebysubject.Name = "selectlecturebysubject";
            this.selectlecturebysubject.Size = new System.Drawing.Size(84, 43);
            this.selectlecturebysubject.TabIndex = 24;
            this.selectlecturebysubject.Text = "Select";
            this.selectlecturebysubject.UseVisualStyleBackColor = false;
            this.selectlecturebysubject.Click += new System.EventHandler(this.selectlecturebysubject_Click);
            // 
            // lecturesubject
            // 
            this.lecturesubject.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lecturesubject.Location = new System.Drawing.Point(104, 242);
            this.lecturesubject.Multiline = true;
            this.lecturesubject.Name = "lecturesubject";
            this.lecturesubject.Size = new System.Drawing.Size(132, 28);
            this.lecturesubject.TabIndex = 23;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(11, 234);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(78, 36);
            this.label14.TabIndex = 22;
            this.label14.Text = "Subject:";
            // 
            // selectlecturebytitle
            // 
            this.selectlecturebytitle.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.selectlecturebytitle.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectlecturebytitle.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectlecturebytitle.Location = new System.Drawing.Point(152, 187);
            this.selectlecturebytitle.Name = "selectlecturebytitle";
            this.selectlecturebytitle.Size = new System.Drawing.Size(84, 43);
            this.selectlecturebytitle.TabIndex = 21;
            this.selectlecturebytitle.Text = "Select";
            this.selectlecturebytitle.UseVisualStyleBackColor = false;
            this.selectlecturebytitle.Click += new System.EventHandler(this.selectlecturebytitle_Click);
            // 
            // lecturetitle
            // 
            this.lecturetitle.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lecturetitle.Location = new System.Drawing.Point(104, 153);
            this.lecturetitle.Multiline = true;
            this.lecturetitle.Name = "lecturetitle";
            this.lecturetitle.Size = new System.Drawing.Size(132, 28);
            this.lecturetitle.TabIndex = 20;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(11, 145);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 36);
            this.label15.TabIndex = 19;
            this.label15.Text = "Title:";
            // 
            // selectlecturebyid
            // 
            this.selectlecturebyid.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.selectlecturebyid.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectlecturebyid.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectlecturebyid.Location = new System.Drawing.Point(152, 91);
            this.selectlecturebyid.Name = "selectlecturebyid";
            this.selectlecturebyid.Size = new System.Drawing.Size(84, 43);
            this.selectlecturebyid.TabIndex = 18;
            this.selectlecturebyid.Text = "Select";
            this.selectlecturebyid.UseVisualStyleBackColor = false;
            this.selectlecturebyid.Click += new System.EventHandler(this.selectlecturebyid_Click);
            // 
            // lectureid
            // 
            this.lectureid.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lectureid.Location = new System.Drawing.Point(104, 57);
            this.lectureid.Multiline = true;
            this.lectureid.Name = "lectureid";
            this.lectureid.Size = new System.Drawing.Size(132, 28);
            this.lectureid.TabIndex = 17;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(11, 49);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(34, 36);
            this.label16.TabIndex = 16;
            this.label16.Text = "Id:";
            // 
            // showlecturetable
            // 
            this.showlecturetable.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.showlecturetable.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showlecturetable.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.showlecturetable.Location = new System.Drawing.Point(17, 339);
            this.showlecturetable.Name = "showlecturetable";
            this.showlecturetable.Size = new System.Drawing.Size(209, 43);
            this.showlecturetable.TabIndex = 15;
            this.showlecturetable.Text = "Show Lecture Table";
            this.showlecturetable.UseVisualStyleBackColor = false;
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.showdepttable);
            this.groupBox6.Controls.Add(this.selectdeptbyname);
            this.groupBox6.Controls.Add(this.deptname);
            this.groupBox6.Controls.Add(this.label17);
            this.groupBox6.Controls.Add(this.selectdeptbyid);
            this.groupBox6.Controls.Add(this.deptid);
            this.groupBox6.Controls.Add(this.label18);
            this.groupBox6.Controls.Add(this.button5);
            this.groupBox6.Font = new System.Drawing.Font("DG Ghayaty", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(899, 664);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox6.Size = new System.Drawing.Size(247, 295);
            this.groupBox6.TabIndex = 21;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Department Table";
            // 
            // showdepttable
            // 
            this.showdepttable.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.showdepttable.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showdepttable.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.showdepttable.Location = new System.Drawing.Point(17, 242);
            this.showdepttable.Name = "showdepttable";
            this.showdepttable.Size = new System.Drawing.Size(209, 43);
            this.showdepttable.TabIndex = 22;
            this.showdepttable.Text = "Show Dept Table";
            this.showdepttable.UseVisualStyleBackColor = false;
            this.showdepttable.Click += new System.EventHandler(this.showdepttable_Click);
            // 
            // selectdeptbyname
            // 
            this.selectdeptbyname.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.selectdeptbyname.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectdeptbyname.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectdeptbyname.Location = new System.Drawing.Point(152, 185);
            this.selectdeptbyname.Name = "selectdeptbyname";
            this.selectdeptbyname.Size = new System.Drawing.Size(84, 43);
            this.selectdeptbyname.TabIndex = 21;
            this.selectdeptbyname.Text = "Select";
            this.selectdeptbyname.UseVisualStyleBackColor = false;
            this.selectdeptbyname.Click += new System.EventHandler(this.selectdeptbyname_Click);
            // 
            // deptname
            // 
            this.deptname.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deptname.Location = new System.Drawing.Point(104, 151);
            this.deptname.Multiline = true;
            this.deptname.Name = "deptname";
            this.deptname.Size = new System.Drawing.Size(132, 28);
            this.deptname.TabIndex = 20;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(11, 143);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 36);
            this.label17.TabIndex = 19;
            this.label17.Text = "Name:";
            // 
            // selectdeptbyid
            // 
            this.selectdeptbyid.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.selectdeptbyid.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectdeptbyid.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectdeptbyid.Location = new System.Drawing.Point(152, 91);
            this.selectdeptbyid.Name = "selectdeptbyid";
            this.selectdeptbyid.Size = new System.Drawing.Size(84, 43);
            this.selectdeptbyid.TabIndex = 18;
            this.selectdeptbyid.Text = "Select";
            this.selectdeptbyid.UseVisualStyleBackColor = false;
            this.selectdeptbyid.Click += new System.EventHandler(this.selectdeptbyid_Click);
            // 
            // deptid
            // 
            this.deptid.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deptid.Location = new System.Drawing.Point(104, 57);
            this.deptid.Multiline = true;
            this.deptid.Name = "deptid";
            this.deptid.Size = new System.Drawing.Size(132, 28);
            this.deptid.TabIndex = 17;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(11, 49);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(34, 36);
            this.label18.TabIndex = 16;
            this.label18.Text = "Id:";
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button5.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button5.Location = new System.Drawing.Point(17, 342);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(209, 43);
            this.button5.TabIndex = 15;
            this.button5.Text = "Show Exam Table";
            this.button5.UseVisualStyleBackColor = false;
            // 
            // backbtn
            // 
            this.backbtn.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.backbtn.Font = new System.Drawing.Font("DG Ghayaty", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backbtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.backbtn.Location = new System.Drawing.Point(12, 944);
            this.backbtn.Name = "backbtn";
            this.backbtn.Size = new System.Drawing.Size(58, 34);
            this.backbtn.TabIndex = 23;
            this.backbtn.Text = "back";
            this.backbtn.UseVisualStyleBackColor = false;
            this.backbtn.Click += new System.EventHandler(this.backbtn_Click);
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(1171, 990);
            this.Controls.Add(this.backbtn);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.dataGridView1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form5";
            this.Text = "select";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button showstudentstablebtn;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox studentid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button selectstudentbyid;
        private System.Windows.Forms.Button selectstudentbyphone;
        private System.Windows.Forms.TextBox studentphone;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button selectstudentbyemail;
        private System.Windows.Forms.TextBox studentemail;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button selectstudentbyusername;
        private System.Windows.Forms.TextBox studentusername;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button selectexambyterm;
        private System.Windows.Forms.TextBox examterm;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button selectexambysubject;
        private System.Windows.Forms.TextBox examsubject;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button selectexambyid;
        private System.Windows.Forms.TextBox examid;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button showexamtable;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button selectmarkbystudent;
        private System.Windows.Forms.TextBox markstudent;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button selectmarkbyexam;
        private System.Windows.Forms.TextBox markexam;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button showmakstable;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button selectsubjectbyterm;
        private System.Windows.Forms.TextBox subjectterm;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button selectsubjectbymindeg;
        private System.Windows.Forms.TextBox subjectmindeg;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button selectsubjectbydept;
        private System.Windows.Forms.TextBox subjectdepartment;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button selectsubjectbyname;
        private System.Windows.Forms.TextBox subjectname;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button showsubjecttable;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button selectlecturebysubject;
        private System.Windows.Forms.TextBox lecturesubject;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button selectlecturebytitle;
        private System.Windows.Forms.TextBox lecturetitle;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button selectlecturebyid;
        private System.Windows.Forms.TextBox lectureid;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button showlecturetable;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button showdepttable;
        private System.Windows.Forms.Button selectdeptbyname;
        private System.Windows.Forms.TextBox deptname;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button selectdeptbyid;
        private System.Windows.Forms.TextBox deptid;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button backbtn;
    }
}