﻿namespace Entity_FrameWork_Project
{
    partial class Form6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form6));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.selectstudentsbydepartment = new System.Windows.Forms.Button();
            this.deptname = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.selectstudentbyexam1 = new System.Windows.Forms.Button();
            this.examid1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.selectstudentsbyexam2 = new System.Windows.Forms.Button();
            this.examid2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.selectsubjectbydepartment = new System.Windows.Forms.Button();
            this.subjectsbydepartment = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.calcavg = new System.Windows.Forms.Button();
            this.username = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.countlectures = new System.Windows.Forms.Button();
            this.subject = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.backbtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(496, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(547, 718);
            this.dataGridView1.TabIndex = 0;
            // 
            // selectstudentsbydepartment
            // 
            this.selectstudentsbydepartment.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.selectstudentsbydepartment.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectstudentsbydepartment.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectstudentsbydepartment.Location = new System.Drawing.Point(12, 64);
            this.selectstudentsbydepartment.Name = "selectstudentsbydepartment";
            this.selectstudentsbydepartment.Size = new System.Drawing.Size(343, 43);
            this.selectstudentsbydepartment.TabIndex = 24;
            this.selectstudentsbydepartment.Text = "Select Students By Department ";
            this.selectstudentsbydepartment.UseVisualStyleBackColor = false;
            this.selectstudentsbydepartment.Click += new System.EventHandler(this.selectstudentsbydepartment_Click);
            // 
            // deptname
            // 
            this.deptname.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deptname.Location = new System.Drawing.Point(223, 30);
            this.deptname.Multiline = true;
            this.deptname.Name = "deptname";
            this.deptname.Size = new System.Drawing.Size(132, 28);
            this.deptname.TabIndex = 23;
            this.deptname.TextChanged += new System.EventHandler(this.deptname_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(23, 22);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(116, 36);
            this.label17.TabIndex = 22;
            this.label17.Text = "Department:";
            // 
            // selectstudentbyexam1
            // 
            this.selectstudentbyexam1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.selectstudentbyexam1.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectstudentbyexam1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectstudentbyexam1.Location = new System.Drawing.Point(12, 165);
            this.selectstudentbyexam1.Name = "selectstudentbyexam1";
            this.selectstudentbyexam1.Size = new System.Drawing.Size(343, 43);
            this.selectstudentbyexam1.TabIndex = 27;
            this.selectstudentbyexam1.Text = "Select Students Who didn\'t sit for exam";
            this.selectstudentbyexam1.UseVisualStyleBackColor = false;
            this.selectstudentbyexam1.Click += new System.EventHandler(this.selectstudentbyexam1_Click);
            // 
            // examid1
            // 
            this.examid1.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.examid1.Location = new System.Drawing.Point(223, 134);
            this.examid1.Multiline = true;
            this.examid1.Name = "examid1";
            this.examid1.Size = new System.Drawing.Size(132, 28);
            this.examid1.TabIndex = 26;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 126);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 36);
            this.label8.TabIndex = 25;
            this.label8.Text = "Exam Id:";
            // 
            // selectstudentsbyexam2
            // 
            this.selectstudentsbyexam2.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.selectstudentsbyexam2.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectstudentsbyexam2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectstudentsbyexam2.Location = new System.Drawing.Point(12, 274);
            this.selectstudentsbyexam2.Name = "selectstudentsbyexam2";
            this.selectstudentsbyexam2.Size = new System.Drawing.Size(343, 43);
            this.selectstudentsbyexam2.TabIndex = 30;
            this.selectstudentsbyexam2.Text = "Select Students Who sitted for exam";
            this.selectstudentsbyexam2.UseVisualStyleBackColor = false;
            this.selectstudentsbyexam2.Click += new System.EventHandler(this.selectstudentsbyexam2_Click);
            // 
            // examid2
            // 
            this.examid2.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.examid2.Location = new System.Drawing.Point(223, 243);
            this.examid2.Multiline = true;
            this.examid2.Name = "examid2";
            this.examid2.Size = new System.Drawing.Size(132, 28);
            this.examid2.TabIndex = 29;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 235);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 36);
            this.label1.TabIndex = 28;
            this.label1.Text = "Exam Id:";
            // 
            // selectsubjectbydepartment
            // 
            this.selectsubjectbydepartment.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.selectsubjectbydepartment.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectsubjectbydepartment.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.selectsubjectbydepartment.Location = new System.Drawing.Point(12, 400);
            this.selectsubjectbydepartment.Name = "selectsubjectbydepartment";
            this.selectsubjectbydepartment.Size = new System.Drawing.Size(343, 43);
            this.selectsubjectbydepartment.TabIndex = 33;
            this.selectsubjectbydepartment.Text = "Select Subjects By Department ";
            this.selectsubjectbydepartment.UseVisualStyleBackColor = false;
            this.selectsubjectbydepartment.Click += new System.EventHandler(this.selectsubjectbydepartment_Click);
            // 
            // subjectsbydepartment
            // 
            this.subjectsbydepartment.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectsbydepartment.Location = new System.Drawing.Point(223, 366);
            this.subjectsbydepartment.Multiline = true;
            this.subjectsbydepartment.Name = "subjectsbydepartment";
            this.subjectsbydepartment.Size = new System.Drawing.Size(132, 28);
            this.subjectsbydepartment.TabIndex = 32;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(23, 358);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 36);
            this.label2.TabIndex = 31;
            this.label2.Text = "Department:";
            // 
            // calcavg
            // 
            this.calcavg.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.calcavg.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcavg.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.calcavg.Location = new System.Drawing.Point(12, 510);
            this.calcavg.Name = "calcavg";
            this.calcavg.Size = new System.Drawing.Size(343, 43);
            this.calcavg.TabIndex = 36;
            this.calcavg.Text = "Calculate Avg";
            this.calcavg.UseVisualStyleBackColor = false;
            this.calcavg.Click += new System.EventHandler(this.calcavg_Click);
            // 
            // username
            // 
            this.username.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.username.Location = new System.Drawing.Point(223, 476);
            this.username.Multiline = true;
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(132, 28);
            this.username.TabIndex = 35;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(23, 468);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 36);
            this.label3.TabIndex = 34;
            this.label3.Text = "Student:";
            // 
            // countlectures
            // 
            this.countlectures.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.countlectures.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.countlectures.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.countlectures.Location = new System.Drawing.Point(12, 635);
            this.countlectures.Name = "countlectures";
            this.countlectures.Size = new System.Drawing.Size(343, 43);
            this.countlectures.TabIndex = 39;
            this.countlectures.Text = "Count Lectures";
            this.countlectures.UseVisualStyleBackColor = false;
            this.countlectures.Click += new System.EventHandler(this.countlectures_Click);
            // 
            // subject
            // 
            this.subject.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subject.Location = new System.Drawing.Point(223, 601);
            this.subject.Multiline = true;
            this.subject.Name = "subject";
            this.subject.Size = new System.Drawing.Size(132, 28);
            this.subject.TabIndex = 38;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(23, 593);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 36);
            this.label4.TabIndex = 37;
            this.label4.Text = "Subject:";
            // 
            // backbtn
            // 
            this.backbtn.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.backbtn.Font = new System.Drawing.Font("DG Ghayaty", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backbtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.backbtn.Location = new System.Drawing.Point(12, 696);
            this.backbtn.Name = "backbtn";
            this.backbtn.Size = new System.Drawing.Size(58, 34);
            this.backbtn.TabIndex = 40;
            this.backbtn.Text = "back";
            this.backbtn.UseVisualStyleBackColor = false;
            this.backbtn.Click += new System.EventHandler(this.backbtn_Click);
            // 
            // Form6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1055, 742);
            this.Controls.Add(this.backbtn);
            this.Controls.Add(this.countlectures);
            this.Controls.Add(this.subject);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.calcavg);
            this.Controls.Add(this.username);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.selectsubjectbydepartment);
            this.Controls.Add(this.subjectsbydepartment);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.selectstudentsbyexam2);
            this.Controls.Add(this.examid2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.selectstudentbyexam1);
            this.Controls.Add(this.examid1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.selectstudentsbydepartment);
            this.Controls.Add(this.deptname);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.dataGridView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form6";
            this.Text = "Advanced";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button selectstudentsbydepartment;
        private System.Windows.Forms.TextBox deptname;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button selectstudentbyexam1;
        private System.Windows.Forms.TextBox examid1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button selectstudentsbyexam2;
        private System.Windows.Forms.TextBox examid2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button selectsubjectbydepartment;
        private System.Windows.Forms.TextBox subjectsbydepartment;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button calcavg;
        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button countlectures;
        private System.Windows.Forms.TextBox subject;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button backbtn;
    }
}