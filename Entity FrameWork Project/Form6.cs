﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Entity_FrameWork_Project
{
    public partial class Form6 : Form
    {
        public Form6()
        {
            InitializeComponent();
        }

        private void deptname_TextChanged(object sender, EventArgs e)
        {

        }

        private void selectstudentsbydepartment_Click(object sender, EventArgs e)
        {
            var students = Student.ViewStudentsByDepartment(deptname.Text);
            if (students != null)
                dataGridView1.DataSource = students;
            else
                MessageBox.Show("not found!");
        }

        private void selectstudentbyexam1_Click(object sender, EventArgs e)
        {
            var studentexams = Student.ViewStudentsWithoutSelectedExam(int.Parse(examid1.Text));
            if (studentexams != null)
                dataGridView1.DataSource = studentexams;
            else
                MessageBox.Show("not found!");
        }

        private void selectstudentsbyexam2_Click(object sender, EventArgs e)
        {
            var studentexams = Student.ViewStudentsWithSelectedExam(int.Parse(examid2.Text));
            if (studentexams != null)
                dataGridView1.DataSource = studentexams;
            else
                MessageBox.Show("not found!");
        }

        private void selectsubjectbydepartment_Click(object sender, EventArgs e)
        {
            var subjects = Subject.ViewSubjectsByDepartment(selectsubjectbydepartment.Text);
            if (subjects != null)
                dataGridView1.DataSource = subjects;
            else
                MessageBox.Show("not found!");
        }

        private void calcavg_Click(object sender, EventArgs e)
        {
            Student.CalculateAvg(username.Text);
        }

        private void countlectures_Click(object sender, EventArgs e)
        {
            Subject.LecturesCount(subject.Text);
        }

        private void backbtn_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            Form1 f1 = new Form1();
            f1.Show();
        }
    }
}
