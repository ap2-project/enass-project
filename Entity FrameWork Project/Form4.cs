﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Entity_FrameWork_Project
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void deletestudentbyid_Click(object sender, EventArgs e)
        {
            Student.DeleteStudentById(int.Parse(studentid.Text));
        }

        private void deletestudetnbyuser_Click(object sender, EventArgs e)
        {
            Student.DeleteByUserName(studentuser.Text);
        }

        private void deletestudentbyemail_Click(object sender, EventArgs e)
        {
            Student.DeleteByEmail(studentemail.Text);
        }

        //Delete Department By Id**
        private void deletedepartmentbyname_Click(object sender, EventArgs e)
        {
            Department.DeleteDepartmentById(int.Parse(departmentbyname.Text));
        }

        private void deletesubjectbyid_Click(object sender, EventArgs e)
        {
            Subject.DeleteSubjectById(int.Parse(subjectid.Text));
        }

        private void deletemarkbyid_Click(object sender, EventArgs e)
        {
            StudentMark.DeleteMarkById(int.Parse(markid.Text));
        }

        private void deletelecturebyid_Click(object sender, EventArgs e)
        {
            SubjectLecture.DeleteLectureById(int.Parse(lectureid.Text));
        }

        private void deletelecturebytitle_Click(object sender, EventArgs e)
        {
            SubjectLecture.DeleteByTitle(lecturetitle.Text);
        }

        private void deleteexambyid_Click(object sender, EventArgs e)
        {
            Exam.DeleteExamById(int.Parse(examid.Text));
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void subjectname_TextChanged(object sender, EventArgs e)
        {

        }

        private void backbtn_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            Form1 f1 = new Form1();
            f1.Show();
        }
    }
}
