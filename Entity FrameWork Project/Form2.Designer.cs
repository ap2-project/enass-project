﻿namespace Entity_FrameWork_Project
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.studentdepartment = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.phone = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.email = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lastname = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.firstname = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.studentaddbtn = new System.Windows.Forms.Button();
            this.username = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.departmentname = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.departmentaddbtn = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.subjectname = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.subjectyear = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.subjectterm = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.subjectmindegree = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.subjectdepartment = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.subjectaddbtn = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lecturecontent = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.lecturetitle = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.lecturesubject = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.lecturesupdatebtn = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.examdate = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.examsubject = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.examterm = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.examaddbtn = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.mark = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.markexam = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.markstudent = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.markaddbtn = new System.Windows.Forms.Button();
            this.backbtn = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.registerdate = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.registerdate);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.studentdepartment);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.phone);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.email);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lastname);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.firstname);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.studentaddbtn);
            this.groupBox1.Controls.Add(this.username);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("DG Ghayaty", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(36, 16);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox1.Size = new System.Drawing.Size(304, 473);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add Student";
            // 
            // studentdepartment
            // 
            this.studentdepartment.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentdepartment.Location = new System.Drawing.Point(133, 361);
            this.studentdepartment.Multiline = true;
            this.studentdepartment.Name = "studentdepartment";
            this.studentdepartment.Size = new System.Drawing.Size(160, 28);
            this.studentdepartment.TabIndex = 22;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(17, 353);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 36);
            this.label7.TabIndex = 21;
            this.label7.Text = "department:";
            // 
            // phone
            // 
            this.phone.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phone.Location = new System.Drawing.Point(133, 264);
            this.phone.Multiline = true;
            this.phone.Name = "phone";
            this.phone.Size = new System.Drawing.Size(160, 28);
            this.phone.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(17, 256);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 36);
            this.label5.TabIndex = 13;
            this.label5.Text = "phone:";
            // 
            // email
            // 
            this.email.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.email.Location = new System.Drawing.Point(133, 216);
            this.email.Multiline = true;
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(160, 28);
            this.email.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(17, 208);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 36);
            this.label4.TabIndex = 17;
            this.label4.Text = "email:";
            // 
            // lastname
            // 
            this.lastname.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lastname.Location = new System.Drawing.Point(133, 164);
            this.lastname.Multiline = true;
            this.lastname.Name = "lastname";
            this.lastname.Size = new System.Drawing.Size(160, 28);
            this.lastname.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 36);
            this.label3.TabIndex = 13;
            this.label3.Text = "last name:";
            // 
            // firstname
            // 
            this.firstname.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstname.Location = new System.Drawing.Point(133, 112);
            this.firstname.Multiline = true;
            this.firstname.Name = "firstname";
            this.firstname.Size = new System.Drawing.Size(160, 28);
            this.firstname.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 36);
            this.label2.TabIndex = 15;
            this.label2.Text = "first name:";
            // 
            // studentaddbtn
            // 
            this.studentaddbtn.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.studentaddbtn.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentaddbtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.studentaddbtn.Location = new System.Drawing.Point(192, 420);
            this.studentaddbtn.Name = "studentaddbtn";
            this.studentaddbtn.Size = new System.Drawing.Size(101, 43);
            this.studentaddbtn.TabIndex = 14;
            this.studentaddbtn.Text = "Add";
            this.studentaddbtn.UseVisualStyleBackColor = false;
            this.studentaddbtn.Click += new System.EventHandler(this.studentaddbtn_Click);
            // 
            // username
            // 
            this.username.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.username.Location = new System.Drawing.Point(133, 65);
            this.username.Multiline = true;
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(160, 28);
            this.username.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 36);
            this.label1.TabIndex = 1;
            this.label1.Text = "user name:";
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.Transparent;
            this.groupBox7.Controls.Add(this.departmentname);
            this.groupBox7.Controls.Add(this.label28);
            this.groupBox7.Controls.Add(this.departmentaddbtn);
            this.groupBox7.Font = new System.Drawing.Font("DG Ghayaty", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(40, 537);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox7.Size = new System.Drawing.Size(304, 189);
            this.groupBox7.TabIndex = 16;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Add Department";
            // 
            // departmentname
            // 
            this.departmentname.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.departmentname.Location = new System.Drawing.Point(133, 70);
            this.departmentname.Multiline = true;
            this.departmentname.Name = "departmentname";
            this.departmentname.Size = new System.Drawing.Size(160, 28);
            this.departmentname.TabIndex = 17;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(14, 62);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(63, 36);
            this.label28.TabIndex = 16;
            this.label28.Text = "name:";
            // 
            // departmentaddbtn
            // 
            this.departmentaddbtn.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.departmentaddbtn.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.departmentaddbtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.departmentaddbtn.Location = new System.Drawing.Point(192, 136);
            this.departmentaddbtn.Name = "departmentaddbtn";
            this.departmentaddbtn.Size = new System.Drawing.Size(101, 43);
            this.departmentaddbtn.TabIndex = 14;
            this.departmentaddbtn.Text = "Add";
            this.departmentaddbtn.UseVisualStyleBackColor = false;
            this.departmentaddbtn.Click += new System.EventHandler(this.departmentaddbtn_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.Transparent;
            this.groupBox8.Controls.Add(this.subjectname);
            this.groupBox8.Controls.Add(this.label30);
            this.groupBox8.Controls.Add(this.subjectyear);
            this.groupBox8.Controls.Add(this.label31);
            this.groupBox8.Controls.Add(this.subjectterm);
            this.groupBox8.Controls.Add(this.label32);
            this.groupBox8.Controls.Add(this.subjectmindegree);
            this.groupBox8.Controls.Add(this.label33);
            this.groupBox8.Controls.Add(this.subjectdepartment);
            this.groupBox8.Controls.Add(this.label34);
            this.groupBox8.Controls.Add(this.subjectaddbtn);
            this.groupBox8.Font = new System.Drawing.Font("DG Ghayaty", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.Location = new System.Drawing.Point(360, 16);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox8.Size = new System.Drawing.Size(318, 378);
            this.groupBox8.TabIndex = 17;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Add Subject";
            // 
            // subjectname
            // 
            this.subjectname.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectname.Location = new System.Drawing.Point(147, 65);
            this.subjectname.Multiline = true;
            this.subjectname.Name = "subjectname";
            this.subjectname.Size = new System.Drawing.Size(160, 28);
            this.subjectname.TabIndex = 24;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(14, 57);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(63, 36);
            this.label30.TabIndex = 23;
            this.label30.Text = "name:";
            // 
            // subjectyear
            // 
            this.subjectyear.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectyear.Location = new System.Drawing.Point(147, 282);
            this.subjectyear.Multiline = true;
            this.subjectyear.Name = "subjectyear";
            this.subjectyear.Size = new System.Drawing.Size(160, 28);
            this.subjectyear.TabIndex = 22;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(14, 276);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(55, 36);
            this.label31.TabIndex = 21;
            this.label31.Text = "Year:";
            // 
            // subjectterm
            // 
            this.subjectterm.AcceptsReturn = true;
            this.subjectterm.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectterm.Location = new System.Drawing.Point(147, 231);
            this.subjectterm.Multiline = true;
            this.subjectterm.Name = "subjectterm";
            this.subjectterm.Size = new System.Drawing.Size(160, 28);
            this.subjectterm.TabIndex = 20;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(14, 225);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(62, 36);
            this.label32.TabIndex = 19;
            this.label32.Text = "Term:";
            // 
            // subjectmindegree
            // 
            this.subjectmindegree.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectmindegree.Location = new System.Drawing.Point(147, 169);
            this.subjectmindegree.Multiline = true;
            this.subjectmindegree.Name = "subjectmindegree";
            this.subjectmindegree.Size = new System.Drawing.Size(160, 28);
            this.subjectmindegree.TabIndex = 14;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(14, 163);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(104, 36);
            this.label33.TabIndex = 13;
            this.label33.Text = "min degree:";
            // 
            // subjectdepartment
            // 
            this.subjectdepartment.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectdepartment.Location = new System.Drawing.Point(147, 112);
            this.subjectdepartment.Multiline = true;
            this.subjectdepartment.Name = "subjectdepartment";
            this.subjectdepartment.Size = new System.Drawing.Size(160, 28);
            this.subjectdepartment.TabIndex = 18;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(17, 104);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(116, 36);
            this.label34.TabIndex = 17;
            this.label34.Text = "Department:";
            // 
            // subjectaddbtn
            // 
            this.subjectaddbtn.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.subjectaddbtn.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectaddbtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.subjectaddbtn.Location = new System.Drawing.Point(206, 325);
            this.subjectaddbtn.Name = "subjectaddbtn";
            this.subjectaddbtn.Size = new System.Drawing.Size(101, 43);
            this.subjectaddbtn.TabIndex = 14;
            this.subjectaddbtn.Text = "Add";
            this.subjectaddbtn.UseVisualStyleBackColor = false;
            this.subjectaddbtn.Click += new System.EventHandler(this.subjectaddbtn_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.lecturecontent);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.lecturetitle);
            this.groupBox5.Controls.Add(this.label19);
            this.groupBox5.Controls.Add(this.lecturesubject);
            this.groupBox5.Controls.Add(this.label21);
            this.groupBox5.Controls.Add(this.lecturesupdatebtn);
            this.groupBox5.Font = new System.Drawing.Font("DG Ghayaty", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(360, 408);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox5.Size = new System.Drawing.Size(318, 318);
            this.groupBox5.TabIndex = 18;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Add Lecture";
            // 
            // lecturecontent
            // 
            this.lecturecontent.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lecturecontent.Location = new System.Drawing.Point(147, 191);
            this.lecturecontent.Multiline = true;
            this.lecturecontent.Name = "lecturecontent";
            this.lecturecontent.Size = new System.Drawing.Size(160, 28);
            this.lecturecontent.TabIndex = 14;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(14, 185);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 36);
            this.label18.TabIndex = 13;
            this.label18.Text = "Content:";
            // 
            // lecturetitle
            // 
            this.lecturetitle.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lecturetitle.Location = new System.Drawing.Point(147, 129);
            this.lecturetitle.Multiline = true;
            this.lecturetitle.Name = "lecturetitle";
            this.lecturetitle.Size = new System.Drawing.Size(160, 28);
            this.lecturetitle.TabIndex = 18;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(17, 121);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(55, 36);
            this.label19.TabIndex = 17;
            this.label19.Text = "Title:";
            // 
            // lecturesubject
            // 
            this.lecturesubject.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lecturesubject.Location = new System.Drawing.Point(147, 70);
            this.lecturesubject.Multiline = true;
            this.lecturesubject.Name = "lecturesubject";
            this.lecturesubject.Size = new System.Drawing.Size(160, 28);
            this.lecturesubject.TabIndex = 14;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(14, 62);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(78, 36);
            this.label21.TabIndex = 13;
            this.label21.Text = "Subject:";
            // 
            // lecturesupdatebtn
            // 
            this.lecturesupdatebtn.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.lecturesupdatebtn.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lecturesupdatebtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lecturesupdatebtn.Location = new System.Drawing.Point(206, 265);
            this.lecturesupdatebtn.Name = "lecturesupdatebtn";
            this.lecturesupdatebtn.Size = new System.Drawing.Size(101, 43);
            this.lecturesupdatebtn.TabIndex = 14;
            this.lecturesupdatebtn.Text = "Add";
            this.lecturesupdatebtn.UseVisualStyleBackColor = false;
            this.lecturesupdatebtn.Click += new System.EventHandler(this.lecturesupdatebtn_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.examdate);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.examsubject);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.examterm);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.examaddbtn);
            this.groupBox3.Font = new System.Drawing.Font("DG Ghayaty", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(717, 16);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox3.Size = new System.Drawing.Size(299, 341);
            this.groupBox3.TabIndex = 19;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Add Exam";
            // 
            // examdate
            // 
            this.examdate.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.examdate.Location = new System.Drawing.Point(128, 130);
            this.examdate.Multiline = true;
            this.examdate.Name = "examdate";
            this.examdate.Size = new System.Drawing.Size(160, 28);
            this.examdate.TabIndex = 25;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(33, 122);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 36);
            this.label12.TabIndex = 24;
            this.label12.Text = "Date:";
            // 
            // examsubject
            // 
            this.examsubject.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.examsubject.Location = new System.Drawing.Point(128, 65);
            this.examsubject.Multiline = true;
            this.examsubject.Name = "examsubject";
            this.examsubject.Size = new System.Drawing.Size(160, 28);
            this.examsubject.TabIndex = 23;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(30, 57);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(78, 36);
            this.label25.TabIndex = 22;
            this.label25.Text = "Subject:";
            // 
            // examterm
            // 
            this.examterm.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.examterm.Location = new System.Drawing.Point(128, 193);
            this.examterm.Multiline = true;
            this.examterm.Name = "examterm";
            this.examterm.Size = new System.Drawing.Size(160, 28);
            this.examterm.TabIndex = 20;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(30, 187);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(62, 36);
            this.label14.TabIndex = 19;
            this.label14.Text = "Term:";
            // 
            // examaddbtn
            // 
            this.examaddbtn.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.examaddbtn.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.examaddbtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.examaddbtn.Location = new System.Drawing.Point(187, 287);
            this.examaddbtn.Name = "examaddbtn";
            this.examaddbtn.Size = new System.Drawing.Size(101, 43);
            this.examaddbtn.TabIndex = 14;
            this.examaddbtn.Text = "Add";
            this.examaddbtn.UseVisualStyleBackColor = false;
            this.examaddbtn.Click += new System.EventHandler(this.examaddbtn_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.mark);
            this.groupBox6.Controls.Add(this.label22);
            this.groupBox6.Controls.Add(this.markexam);
            this.groupBox6.Controls.Add(this.label23);
            this.groupBox6.Controls.Add(this.markstudent);
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Controls.Add(this.markaddbtn);
            this.groupBox6.Font = new System.Drawing.Font("DG Ghayaty", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(717, 387);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.groupBox6.Size = new System.Drawing.Size(299, 339);
            this.groupBox6.TabIndex = 20;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Add Mark";
            // 
            // mark
            // 
            this.mark.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mark.Location = new System.Drawing.Point(130, 194);
            this.mark.Multiline = true;
            this.mark.Name = "mark";
            this.mark.Size = new System.Drawing.Size(160, 28);
            this.mark.TabIndex = 14;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(11, 188);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(60, 36);
            this.label22.TabIndex = 13;
            this.label22.Text = "Mark:";
            // 
            // markexam
            // 
            this.markexam.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.markexam.Location = new System.Drawing.Point(130, 135);
            this.markexam.Multiline = true;
            this.markexam.Name = "markexam";
            this.markexam.Size = new System.Drawing.Size(160, 28);
            this.markexam.TabIndex = 18;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(11, 127);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(61, 36);
            this.label23.TabIndex = 17;
            this.label23.Text = "Exam:";
            // 
            // markstudent
            // 
            this.markstudent.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.markstudent.Location = new System.Drawing.Point(130, 75);
            this.markstudent.Multiline = true;
            this.markstudent.Name = "markstudent";
            this.markstudent.Size = new System.Drawing.Size(160, 28);
            this.markstudent.TabIndex = 14;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(11, 67);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(76, 36);
            this.label24.TabIndex = 13;
            this.label24.Text = "Student";
            // 
            // markaddbtn
            // 
            this.markaddbtn.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.markaddbtn.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.markaddbtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.markaddbtn.Location = new System.Drawing.Point(187, 286);
            this.markaddbtn.Name = "markaddbtn";
            this.markaddbtn.Size = new System.Drawing.Size(101, 43);
            this.markaddbtn.TabIndex = 14;
            this.markaddbtn.Text = "Add";
            this.markaddbtn.UseVisualStyleBackColor = false;
            this.markaddbtn.Click += new System.EventHandler(this.markaddbtn_Click);
            // 
            // backbtn
            // 
            this.backbtn.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.backbtn.Font = new System.Drawing.Font("DG Ghayaty", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backbtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.backbtn.Location = new System.Drawing.Point(12, 696);
            this.backbtn.Name = "backbtn";
            this.backbtn.Size = new System.Drawing.Size(58, 34);
            this.backbtn.TabIndex = 21;
            this.backbtn.Text = "back";
            this.backbtn.UseVisualStyleBackColor = false;
            this.backbtn.Click += new System.EventHandler(this.backbtn_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("DG Ghayaty", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(17, 305);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(121, 36);
            this.label6.TabIndex = 23;
            this.label6.Text = "register year:";
            // 
            // registerdate
            // 
            this.registerdate.Font = new System.Drawing.Font("DG Ghayaty", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.registerdate.Location = new System.Drawing.Point(133, 313);
            this.registerdate.Multiline = true;
            this.registerdate.Name = "registerdate";
            this.registerdate.Size = new System.Drawing.Size(160, 28);
            this.registerdate.TabIndex = 24;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(1055, 742);
            this.Controls.Add(this.backbtn);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form2";
            this.Text = "Add";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox studentdepartment;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox phone;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox email;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox lastname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox firstname;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button studentaddbtn;
        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox departmentname;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button departmentaddbtn;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox subjectname;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox subjectyear;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox subjectterm;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox subjectmindegree;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox subjectdepartment;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Button subjectaddbtn;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox lecturecontent;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox lecturetitle;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox lecturesubject;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button lecturesupdatebtn;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox examdate;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox examsubject;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox examterm;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button examaddbtn;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox mark;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox markexam;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox markstudent;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button markaddbtn;
        private System.Windows.Forms.Button backbtn;
        private System.Windows.Forms.TextBox registerdate;
        private System.Windows.Forms.Label label6;
    }
}